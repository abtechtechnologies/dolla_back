<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Los_Angeles');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'HomeController';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'HomeController';

if (!isset($_SERVER['REQUEST_URI'])) {
	$converted_page = 'index';
	$final_uri = 'index';
} else {
	//***********************************!
	//Convert current URI to usable route
	//***********************************!
	$parts = explode("/", $_SERVER['REQUEST_URI'], 3);
	
	if(isset($parts[2])) {
		
		$current_route = $parts[2];
	} else {
		$current_route = $_SERVER['REQUEST_URI'];
	}
	
	$page = $current_route;
	$converted_uri = lcfirst(implode('', array_map('ucfirst', explode('-', $page))));

	if (strpos($page, "_") === 0) {
		//Find position of next '/'
		$dash_position = strpos($page, '/');
		
		if ($dash_position != false) {
			//This is a client
			$client_db_name = substr($page, 1, $dash_position - 1);
		} else {
			//This is a client
			$client_db_name = substr($page, 1);
		}
		
		$this->load->model('client');
		
		$match = $this->client->getRecordByDbName($client_db_name);
		
		if (count($match) > 0) {
			$_SESSION['client_id'] = $match['id'];
		} else {
			$_SESSION['client_id'] = 0;
		}
	} else {
		$_SESSION['client_id'] = 1;
	}
	
	if (strpos($page, '?') === false) {
		$converted_page = $page;
		$final_uri = $converted_uri;
	} else {
		$converted_page = substr($page, 0, strpos($page, '?'));
		$final_uri = substr($converted_uri, 0, strpos($converted_uri, '?'));
	}
	
	if (substr($converted_page, 0, 1) == '/') {
		$converted_page =  substr($converted_page, 1);
		$final_uri =  substr($final_uri, 1);
	}
	
	if (isset($_SESSION['client_id']) && $_SESSION['client_id'] > 0) {
		if (strpos($converted_page, '/') > 0) {
			$converted_page = substr($converted_page, strpos($converted_page, '/') + 1);
		}
		
		if (strpos($final_uri, '/') > 0) {
			$final_uri = substr($final_uri, strpos($final_uri, '/') + 1);
		}
	}

}

$pages_to_route = array(
	'admin',
	'home',
	'about',
	'account',
	'blog',
	'cannabis',
	'community',
	'free-tree',
	'products',
	'login',
	'floral',
	'rec',
	'education',
	'career',
	'partner',
	'code',
	'media',
	'menu',
	'membership',
	'beta',
	'join',
	'sign-up',
	'store',
	'uploader',
	'email',
	'terms',
	'cron',
	'deal',
	'test'
);

foreach ($pages_to_route as $route_root) {
	
	if (strpos($route_root, '-') > 0) {

		$string = explode('-', $route_root);
		
		$formatted_route = '';
		
		foreach ($string as $i => $part) {
			$formatted_route .= ucfirst($part);
		}
	} else {
		$formatted_route = $route_root;
	}
	
	$route[$route_root] = ucfirst($formatted_route) . 'Controller';
	$route[$route_root . '/(:any)'] = ucfirst($formatted_route) . 'Controller/$1';
	$route[$route_root . '/' . $converted_page] = ucfirst($formatted_route) . 'Controller/' . $final_uri;
}

/**** CLIENTS SUB PAGES - DELETE FOR PRODUCTION SITES ****/
$route['_potters'] = 'HomeController';
$route['_potters/(:any)'] = 'HomeController/$1';
$route['_potters/' . $converted_page] = 'HomeController/' . $final_uri;

foreach ($pages_to_route as $route_root) {
	
	if (strpos($route_root, '-') > 0) {
		
		$string = explode('-', $route_root);
		
		$formatted_route = '';
		
		foreach ($string as $i => $part) {
			$formatted_route .= ucfirst($part);
		}
	} else {
		$formatted_route = $route_root;
	}
	
	$route['_potters/' . $route_root] = ucfirst($formatted_route) . 'Controller';
	$route['_potters/' . $route_root . '/(:any)'] = ucfirst($formatted_route) . 'Controller/$1';
	$route['_potters/' . $route_root . '/' . $converted_page] = ucfirst($formatted_route) . 'Controller/' . $final_uri;
}
/**** END OF CLIENTS SUB PAGES - DELETE FOR PRODUCTION SITES ****/

//If uri is not is approved list, serve up 404
if (!array_key_exists((string)$final_uri, $route)) {
	$route[$final_uri] = '_404Controller';
}

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;