<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccountController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}

	public function index()
	{		
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$this->page_data['company_info'] = $this->companyinfo->getRecord();
		$this->page_data['admin'] = 0;
		
		if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0) {
			
			if (!isset($_SESSION['vendor_id']) || $_SESSION['vendor_id'] == 0) {
				//Don't allow access without a logged in user
				redirect('http://' . $this->page_data['company_info']['site']);
			} else {
				$this->page_data['vendor_id'] = $_SESSION['vendor_id'];
				$this->page_data['user_id'] = 0;
				
				$this->load->model('vendor');
				$this->page_data['vendor'] = $this->vendor->getRecord($this->page_data['vendor_id']);
			}
			
			$this->load->model('deal');
			$deals = $this->deal->getRecordsByVendorId($this->page_data['vendor_id']);
			
			$deal_string = '';
			
			foreach ($deals as $deal_id => $deal_data) {
				$deal_string .= $deal_id . ',';
			}
			
			$deal_string = substr($deal_string, 0, -1);
			
			if ($deal_string == '') {
				$this->page_data['requests'] = array();
			} else {
				$this->load->model('dealrequest');
				$this->page_data['requests'] = $this->dealrequest->getRecordsByString($deal_string);
			}
		} else {
			$this->page_data['user_id'] = $_SESSION['user_id'];
			$this->page_data['vendor_id'] = 0;
			$this->page_data['requests'] = array();
			
			$this->load->model('user');
			$this->page_data['user'] = $this->user->getRecord($this->page_data['user_id']);
			
			if ($this->page_data['user']['user_type_id'] == 1) {
				$this->page_data['admin'] = 1;
				
				$this->load->model('dealuse');
				$uses = $this->dealuse->getRecords();
				
				$this->load->model('deal');
				
				$total = 0;
				
				foreach ($uses as $id => $data) {
					$deal = $this->deal->getRecord($data['deal_id']);
					
					$total += $deal['value'];

				}
				
				$this->page_data['admin_total'] = $total;
			}
		}
		
		$this->page_data['signup'] = $this->input->get('signup');
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function deals()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$this->page_data['company_info'] = $this->companyinfo->getRecord();
			
		if (!isset($_SESSION['vendor_id']) || $_SESSION['vendor_id'] == 0) {
			//Don't allow access without a logged in user
			redirect('http://' . $this->page_data['company_info']['site']);
		}
			
		$this->page_data['vendor_id'] = $_SESSION['vendor_id'];
			
		$this->load->model('vendor');
		$this->page_data['vendor'] = $this->vendor->getRecord($this->page_data['vendor_id']);
		
		$this->load->model('deal');
		$this->page_data['deals'] = $this->deal->getRecordsByVendorId($this->page_data['vendor_id']);

		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function deal()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$this->page_data['company_info'] = $this->companyinfo->getRecord();
		
		if (!isset($_SESSION['vendor_id']) || $_SESSION['vendor_id'] == 0) {
			//Don't allow access without a logged in user
			redirect('http://' . $this->page_data['company_info']['site']);
		}
		
		$this->page_data['vendor_id'] = $_SESSION['vendor_id'];
		
		$this->load->model('vendor');
		$this->page_data['vendor'] = $this->vendor->getRecord($this->page_data['vendor_id']);
		
		$this->load->model('deal');
		$this->page_data['deal'] = $this->deal->getRecord($this->input->get('deal_id'));
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function editDeal()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$this->page_data['company_info'] = $this->companyinfo->getRecord();
		
		$this->load->model('deal');
		$this->page_data['deal_id'] = $this->input->get('deal_id');
		
		if ($this->page_data['deal_id'] == '') {
			$this->page_data['deal_id'] = 0;
			$this->page_data['deal'] = array(
				'title' => '',
				'expire' => '',
				'desscription' => '',
				'image' => '',
				'original_price' => '',
				'value' => '',
				'value_type' => 0,
				'deal_type_id' => 1
			);
		} else {
			$this->page_data['deal'] = $this->deal->getRecord($this->page_data['deal_id']);
		}

		if (!isset($_SESSION['vendor_id']) || $_SESSION['vendor_id'] == 0) {
			//Don't allow access without a logged in user
			redirect('http://' . $this->page_data['company_info']['site']);
		} else {
			$this->page_data['vendor_id'] = $_SESSION['vendor_id'];
			
			$this->load->model('vendor');
			$this->page_data['vendor'] = $this->vendor->getRecord($this->page_data['vendor_id']);
		}
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function edit()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$this->page_data['company_info'] = $this->companyinfo->getRecord();
	
		if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0) {
			//Don't allow access without a logged in user
			redirect('http://' . $this->page_data['company_info']['site']);
		}
	
		$this->load->model('user');
		$this->page_data['user'] = $this->user->getRecord($_SESSION['user_id']);
	
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function gift()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
	    $this->load->model('companyinfo');
	    $this->page_data['company_info'] = $this->companyinfo->getRecord();
	
	    if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0) {
	        //Don't allow access without a logged in user
	        redirect('http://' . $this->page_data['company_info']['site']);
	    }
	
	    $this->load->model('user');
	    $this->page_data['user'] = $this->user->getRecord($_SESSION['user_id']);
	
	    //Init functions and page load
	    $this->load->model('_loader');
	    $this->_loader->load($this->page_data);
	}
	
	public function updateInfoAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$name = explode(' ', $_POST['name']);
		
		$this->load->model('user');
		$user = $this->user->getRecord($_SESSION['user_id']);

		$status = $this->user->writeData($_SESSION['user_id'], $name[0], $name[1], $user['email'], 2, $_POST['phone'], '', $_POST['address'], $_POST['city'], $_POST['zip']);
		
		print $status;
		exit;
	}
	
	public function updateVendorAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$name = $_POST['name'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$address = $_POST['address'];
		$website = $_POST['website'];
		$facebook = $_POST['facebook'];
		$instagram = $_POST['instagram'];
		$twitter = $_POST['twitter'];
				
		$this->load->model('vendor');
		$vendor = $this->vendor->getRecord($_SESSION['vendor_id']);
		
		$status = $this->vendor->writeData($vendor['id'], $name, $vendor['title'], $phone, $phone, $address, $vendor['description'], $website, $facebook, $twitter, $instagram, $vendor['vendor_type_id'], $email);
		
		print $status;
		exit;
	}
	
	public function writeDealAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();

		$status = 1;
		
		$maxsize = 2097152;
		
		if ($_FILES['image']['size'] < $maxsize) {
			
			$this->load->model('vendor');
			$vendor = $this->vendor->getRecord($_POST['vendor_id']);
			
			$this->load->model('companyinfo');
			$company = $this->companyinfo->getRecord();
			
			$this->load->model('_utility');
			$path = "./uploads/clients/" . $company['db_name'] . '/vendors/' . $vendor['file_name'] . '/';
			
			$target_path = $this->_utility->cleanFileName(basename('DEAL-' . time() . '-' . $_FILES['image']['name']));
			
			$complete_path = $path . $target_path;

			$status = move_uploaded_file($_FILES['image']['tmp_name'], $complete_path);
	
			$this->load->model('deal');
			//$deal_id, $vendor_id, $title, $expire, $description, $image, $original_price, $value, $value_type
			$status = $this->deal->writeData(0, $_POST['vendor_id'], $_POST['title'], $_POST['expire'], $_POST['description'], $target_path, $_POST['original_price'], $_POST['value'], $_POST['value_type']);
			
		} else {
			$status = 'File must be smaller the 2MB.';
		}
		
		print $status;
		exit;
	}
	
	public function sendReferral () {
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$this->load->model('user');
		$user = $this->user->getRecord($_SESSION['user_id']);
		
		//Check to make sure the user didnt enter his/her own email.... Stoners...		
		$users = $this->user->getUsers();
		$found = 0;
		
		foreach ($users as $id => $data) {
			if ($data['email'] == $_POST['email']) {
				$found = $id;
			}
		}
		
		if ($found > 0) {
			if ($user['email'] == $_POST['email']) {
				$status = 'Sooo, you are trying to referr yourself? No dice.';
			} else {
				$status = 'That email address is already in our system.';
			}
		} else {
			$this->load->library('email');
			$this->email->set_mailtype("html");
				
			$this->email->from('info@wwweed.org', $company['name']);
			
			$this->email->DKIM_domain = $company['site'];
			$this->email->DKIM_private = '../../../config/p_key.txt';
			$this->email->DKIM_selector = 'phpmailer';
			$this->email->DKIM_passphrase = '';
			$this->email->DKIM_identity = 'info@wwweed.org';
				
			$this->email->to($_POST['email']);
			//$this->email->cc('another@another-example.com');
			//$this->email->bcc('them@their-example.com');
			
			$name = ucfirst($user['first_name']) . ' ' . ucfirst(substr($user['last_name'], 0, 1));
				
			$this->email->subject($name . ' has invited to you join WWWEED.ORG!');
				
			$data = array(
				'name' => $name
			);
				
			$body = $this->load->view('email/referral.phtml', $data, TRUE);
			$this->email->message($body);
				
			$this->email->send();
				
			$status = $this->email->print_debugger();
			
			if (trim(strip_tags($status)) == 0) {
				$this->load->model('referral');
				$status = $this->referral->writeData($_SESSION['user_id'], $_POST['email']);
			} else {
				$status = 'There was an error sending the email. Please try again.';
			}
		}
					
		print $status;
		exit;
	}
	
	public function sendGift () {
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
	    $this->load->model('companyinfo');
	    $company = $this->companyinfo->getRecord();
	    
	    do {
    	    if ($_POST['friend_user_id'] == $_SESSION['user_id']) {
    	        $status = 'Sooo, you are trying to gift yourself? No dice.';
    	        break;
    	    }
    	
    	    if ($_POST['friend_user_id'] == 0 || $_POST['friend_user_id'] < 0 || $_POST['friend_user_id'] == 1 || $_POST['friend_user_id'] == 2) {
    	        $status = 'Not a valid user';
    	        break;
    	    }
    	    
    	    $this->load->model('user');
    	    $user = $this->user->getRecord($_SESSION['user_id']);
    	    
    	    //Check to make sure the user didnt enter his/her own ID.... Stoners...
    	    $users = $this->user->getUsers();
    	    $found = 0;
    	
    	    foreach ($users as $id => $data) {
    	        if ($id == $_POST['friend_user_id']) {
    	            $found = $id;
    	        }
    	    }
    	
    	    //ID exists
    	    if ($found > 0) {
    	        $status = $this->user->moveBalance($_SESSION['user_id'], $_POST['friend_user_id'], $_POST['amount']);
//     	        $this->load->library('email');
//     	        $this->email->set_mailtype("html");
    	         
//     	        $this->email->from('info@wwweed.org', $company['name']);
    	        
//     	        $this->email->DKIM_domain = $company['site'];
//     	        $this->email->DKIM_private = '../../../config/p_key.txt';
//     	        $this->email->DKIM_selector = 'phpmailer';
//     	        $this->email->DKIM_passphrase = '';
//     	        $this->email->DKIM_identity = 'info@wwweed.org';
    	         
//     	        $this->email->to($_POST['email']);
//     	        //$this->email->cc('another@another-example.com');
//     	        //$this->email->bcc('them@their-example.com');
    	        
//     	        $name = ucfirst($user['first_name']) . ' ' . ucfirst(substr($user['last_name'], 0, 1));
    	         
//     	        $this->email->subject($name . ' has invited to you join WWWEED.ORG!');
    	         
//     	        $data = array(
//     	                'name' => $name
//     	        );
    	         
//     	        $body = $this->load->view('email/referral.phtml', $data, TRUE);
//     	        $this->email->message($body);
    	         
//     	        $this->email->send();
    	         
//     	        $status = $this->email->print_debugger();
    	        
//     	        if (trim(strip_tags($status)) == 0) {
//     	            $this->load->model('referral');
//     	            $status = $this->referral->writeData($_SESSION['user_id'], $_POST['email']);
//     	        } else {
//     	            $status = 'There was an error sending the email. Please try again.';
//     	        }

    	        
    	    } else {
    	        $status = 'That ID is not found in our system';
    	        break;
    	    }
	    } while (false);
	    	
	    print $status;
	    exit;
	}
}