<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DealController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{		
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();

		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function vendor()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->page_data['vendor_id'] = $this->input->get('vendor_id');
		
		$this->load->model('user');
		$this->page_data['users'] = $this->user->getRecords();
		
		$this->load->model('vendor');
		$this->page_data['vendor'] = $this->vendor->getRecord($this->page_data['vendor_id']);
		
		$this->load->model('review');
		$reviews = $this->review->getRecordsByVendorId($this->page_data['vendor_id']);
		
		
		$this->page_data['reviews'] = $reviews;
		
		$sum = 0;
		
		foreach ($reviews as $review_id => $review_record) {
			$sum += $review_record['rating'];
		}
		
		if (count($reviews) == 0) {
			$avg = 0;
		} else {
			$avg = $sum / count($reviews);
		}
		
		$this->page_data['avg'] = $avg;
		$this->page_data['review_count'] = count($reviews);
		
		$this->load->model('deal');
		$this->page_data['deals'] = $this->deal->getRecordsByVendorId($this->page_data['vendor_id']);
		
		$this->load->model('dealuse');
		
		foreach ($this->page_data['deals'] as $id => $data) {
			$this->page_data['deals'][$id]['used'] = $this->dealuse->getUses($id);
		}

		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function checkout()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function wishlist()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function requests()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('deal');
		$deals = $this->deal->getRecordsByVendorId($_SESSION['vendor_id']);
		
		$deal_string = '';
		
		foreach ($deals as $deal_id => $deal_data) {
			$deal_string .= $deal_id . ',';
		}
		
		$deal_string = substr($deal_string, 0, -1);
		
		$this->load->model('dealrequest');
		$this->page_data['requests'] = $this->dealrequest->getRecordsByString($deal_string);
		
		$this->load->model('user');
		
		foreach ($this->page_data['requests'] as $id => $data) {
			$this->page_data['requests'][$id]['deal'] = $this->deal->getRecord($data['deal_id']);
			$this->page_data['requests'][$id]['user'] = $this->user->getRecord($data['user_id']);
		}
		
		$this->load->model('vendor');
		$this->page_data['vendor'] = $this->vendor->getRecord($_SESSION['vendor_id']);

		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function addDealRequestAction() {
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$user_id = $_SESSION['user_id'];
		$deal_id = $_POST['deal_id'];
		
		$this->load->model('dealrequest');
		$status = $this->dealrequest->writeData(0, $user_id, $deal_id);
		
		print $status;
		exit;
	}
	
	public function acceptRequestAction() {
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$user_id = $_POST['user_id'];
		$deal_id = $_POST['deal_id'];
		$deal_request_id = $_POST['deal_request_id'];
		
		$this->load->model('dealuse');
		$status = $this->dealuse->writeData(0, $user_id, $deal_id);
		
		if ($status == 1) {
			$this->load->model('dealrequest');
			$status = $this->dealrequest->delete($deal_request_id);
			
			if ($status == 1) {
				$this->load->model('deal');
				$deal = $this->deal->getRecord($deal_id);
				
				$this->load->model('user');
				$user = $this->user->getRecord($user_id);
				
				$balance = $user['balance'] + $deal['value'];
				
				$status = $this->user->writeBalance($user_id, $balance);
			}
		}
		
		print $status;
		exit;
	}
	
	public function cart()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function dealGrid()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function dealList()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
}