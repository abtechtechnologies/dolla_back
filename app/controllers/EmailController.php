<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmailController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
	}

	public function index()
	{
		$this->load->helper('url');
		
		$page['page'] = 'email/index';

		//Init functions and page load
		$this->load->model('_loader');
		$page['loader'] = $this->_loader->load($page);
	}
	
	public function sendContact()
	{	
		$this->load->library('email');
		$this->email->set_mailtype("html");

		$this->email->from(
			$this->input->post('email')
		);
		
		$this->email->to('ahartjoe@gmail.com');
		//$this->email->cc('another@another-example.com');
		//$this->email->bcc('them@their-example.com');
		
		$this->email->subject('Received email from flowersonflowers.com');
		
		$data = array(
			'name' => $this->input->post('name'),
			'message' => $this->input->post('message'),
		);
		
		$body = $this->load->view('email/contact.phtml', $data, TRUE);
		$this->email->message($body);
	
		$this->email->send();
		
		$status = $this->email->print_debugger();

		if (trim(strip_tags($status)) == 0) {
			$status = 1;
		}
	
		print strip_tags($status);
	}
	
	public function logError()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();

		$this->load->model('companyinfo');
		$company_info = $this->companyinfo->getRecord();

		$this->load->library('email');
		$this->email->set_mailtype("html");
		
		$this->email->from('error@budbase.io', 'BudBase Error Logs');
		
		$this->email->to('apogeeerror@gmail.com');
		
		$recip = array(
			'apogeereports@gmail.com'
		);
		
		$this->email->bcc($recip);
		
		$this->email->subject('SITE ERROR - ' . $company_info['name']);
		
		$email_data = array(
			'message' => $_POST['message'],	
			'company' => $company_info,	
		);
		
		$body = $this->load->view('email/log-error.phtml', $email_data, TRUE);
		$this->email->message($body);
		
		$this->email->send();
		
		$status = $this->email->print_debugger();
		
		if (trim(strip_tags($status)) == 0) {
			$status = 1;
		}
		
		print $status;
		exit;
	}
	
	public function placeOrder()
	{
		$this->load->library('email');
		$this->email->set_mailtype("html");

		$this->load->model('user');
		$page['user'] = $this->user->getRecord($this->input->post('user_id'));

		$this->email->from(
			$page['user']['email']
		);
	
		$this->email->to('flowersonflowers.sd@gmail.com');
		//$this->email->cc('another@another-example.com');
		//$this->email->bcc('them@their-example.com');
	
		$this->email->subject('Received ORDER(!) from flowersonflowers.com');
	
		$data = array(
			'flower' => $this->input->post('flower'),
			'strain' => $this->input->post('strain'),
			'quantity' => $this->input->post('quantity'),
			'price' => $this->input->post('price'),
			'customer_name' => $page['user']['first_name'] . ' ' . $page['user']['last_name'],
			'phone' => $page['user']['phone'],
			'address' => $page['user']['address'],
		);
	
		$body = $this->load->view('email/place-order.phtml', $data, TRUE);
		$this->email->message($body);
	
		$this->email->send();
	
		$status = $this->email->print_debugger();
	
		if (trim(strip_tags($status)) == 0) {
			$status = 1;
		}
	
		print strip_tags($status);
	}
}