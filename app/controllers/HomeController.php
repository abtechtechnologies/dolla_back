<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function sitemap()
	{
		$this->load->view('home/urllist.txt');
	}

	public function index()
	{		
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();

		//Set # of visits to home page views for now
		$this->load->model('pagecount');
		$status = $this->pagecount->increaseCount();
		
		$this->page_data['signup'] = $this->input->get('signup');
		
		$this->load->model('dealtype');
		$this->page_data['deal_types'] = $this->dealtype->getRecords();
		
		$this->load->model('deal');
		$this->page_data['all_deals'] = $this->deal->getRecords();
		$this->page_data['deals'] = $this->deal->getRecordsByDealType();

		$this->load->model('vendor');
		$this->page_data['featured_vendors'] = $this->vendor->getFeatured();
		
		$this->load->model('review');
		
		foreach ($this->page_data['featured_vendors'] as $id => $data) {
			
			$reviews = $this->review->getRecordsByVendorId($id);
			
			$sum = 0;

			foreach ($reviews as $review_id => $review_record) {
				$sum += $review_record['rating'];
			}
			
			if (count($reviews) == 0) {
				$avg = 0;
			} else {
				$avg = $sum / count($reviews);
			}
			
			$this->page_data['featured_vendors'][$id]['avg'] = $avg;
			$this->page_data['featured_vendors'][$id]['review_count'] = count($reviews);
		}

		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function faq()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();

		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function signin()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->page_data['alert'] = $this->input->get('alert');
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function signinVendor()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->page_data['alert'] = $this->input->get('alert');
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function signup()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function signupVendor()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('vendortype');
		$this->page_data['vendor_types'] = $this->vendortype->getRecords();
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function signupAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$email = $_POST['email'];
		$unlock = $_POST['unlock'];
		
		$status = 1;
		
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->load->model('user');
			$match = $this->user->getUserByEmail($email);
			
			if (count($match) > 0) {
				$status = 'That email address is already in use.';
			} else {
				$status = $this->user->writeData(0, '', '', $email, 2);
				
				if ($status == 1) {
					$status = $this->user->writePassword($unlock);
					
					if ($status == 1) {
						$_SESSION['vendor_id'] = 0;
						$_SESSION['user_id'] = $this->user->getRecentId();
					}
				} else {
					$status = 'Error writing user to database.';
				}
			}
		} else {
			$status = 'That is not a properly formatted email address.';
		}
		
		print $status;
		exit;
	}
	
	public function signupVendorAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$email = $_POST['email'];
		$unlock = $_POST['unlock'];
		$name = $_POST['name'];
		$vendor_type_id = $_POST['vendor_type_id'];
		
		$status = 1;
		
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->load->model('vendor');
			$match = $this->vendor->getRecordByEmail($email);

			if (count($match) > 0) {
				$status = 'You can not use that email address.';
			} else {
				$status = $this->vendor->signUp($email, $unlock, $name, $vendor_type_id);
				
				if ($status == 1) {
					$_SESSION['user_id'] = 0;
					$_SESSION['vendor_id'] = $this->vendor->getRecentId();
				} else {
					$status = 'Error writing vendor to database.';
				}
			}
		} else {
			$status = 'That is not a properly formatted email address.';
		}
		
		print $status;
		exit;
	}
	
	public function signinAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$email = $_POST['email'];
		$unlock = $_POST['unlock'];
		
		$status = 1;
		
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->load->model('user');
			$match = $this->user->getUserByEmail($email);
			
			if (count($match) > 0) {
				if (strtolower($match['email']) == strtolower($email) || hash_equals($match['password'], crypt($unlock, $match['password']))) {
					//User found
					$_SESSION['user_id'] = $match['id'];
					$_SESSION['vendor_id'] = 0;
				}
			} else {
				$this->load->model('vendor');
				$match = $this->vendor->getRecordByEmail($email);
				
				if (count($match) > 0) {
					//Vendor found
					if (strtolower($match['email']) == strtolower($email) || hash_equals($match['password'], crypt($unlock, $match['password']))) {
						//Password Match
						$_SESSION['user_id'] = 0;
						$_SESSION['vendor_id'] = $match['id'];
					} else {
						$status = 'Invalid login credentials';
					}
				} else {
					$status = 'Email address not found';
				}
			}
		} else {
			$status = 'That is not a properly formatted email address.';
		}
		
		print $status;
		exit;
		
// 		if (isset($_SESSION['temp_cart']) && count($_SESSION['temp_cart']) > 0) {
// 			$this->load->model('cart');
// 			$cart = $this->cart->getRecordsByUserId($user_id);
// 			$deal_ids = array();
			
// 			foreach ($cart as $id => $data) {
// 				$deal_ids[$data['deal_id']] = $data['deal_id'];
// 			}
			
// 			$status = 1;
			
// 			foreach ($_SESSION['temp_cart'] as $id => $data) {
// 				if (isset($deal_ids[$id])) {
					
// 				} else {
// 					$this->load->model('deal');
// 					$deal = $this->deal->getRecord($id);
					
// 					$status = $this->cart->writeData($user_id, $deal, $data['quantity']);
// 				}
// 			}
			
// 			if ($status == 1) {
// 				$_SESSION['temp_cart'] = array();
// 			}
// 		}
	}
	
	public function terms()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function dealSingle()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
// 		if (!file_exists('./uploads/clients/dollaback/vendors/joes_deli')) {
// 			$status = mkdir('./uploads/clients/dollaback/vendors/joes_deli', 0777, true);
// 		}

		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function viewDeals()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('vendor');
		$this->page_data['vendors'] = $this->vendor->getRecords();
		
		if ($this->input->get('deal_type_id') == '') {
			$this->load->model('deal');
			$this->page_data['deals'] = $this->deal->getRecords();
		} else {
			$this->load->model('deal');
			$this->page_data['deals'] = $this->deal->getRecordsByDealTypeId($this->input->get('deal_type_id'));
		}
		
		$this->load->model('dealuse');
		$this->load->model('review');
		
		foreach ($this->page_data['deals'] as $id => $data) {
			$this->page_data['deals'][$id]['used'] = $this->dealuse->getUses($id);
			$reviews = $this->review->getRecordsByVendorId($data['vendor_id']);
			$this->page_data['deals'][$id]['reviews'] = count($reviews);
		}

		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function test()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Set # of visits to home page views for now
		$this->load->model('pagecount');
		$status = $this->pagecount->increaseCount();
		
		$this->page_data['signup'] = $this->input->get('signup');
		
		$this->load->model('producttype');
		$this->page_data['product_types'] = $this->producttype->getRecords(true);
		
		$this->load->model('product');
		$this->page_data['sub_products'] = $this->product->getProductSizes();
		
		foreach ($this->page_data['product_types'] as $id => $data) {
			$products = $this->product->getProductsByProductType($id);
			
			foreach ($products as $id2 => $data2) {
				$this->page_data['products'][$id][$id2] = $data2;
			}
		}
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function updateImageAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$status = 1;
		
		$this->load->model('companyinfo');
		$company_info = $this->companyinfo->getRecord();
		
		$maxsize = 2097152;
		
		if ($_FILES['profile_image']['size'] < $maxsize) {
			if ($_POST['user_id'] > 0) {
				$this->load->model('user');
				$user = $this->user->getRecord($_POST['user_id']);
				
				$email = explode('@', $user['email']);
				
				$this->load->model('_utility');
				
				$path = "./uploads/clients/" . $company_info['db_name'] . '/users/';
				
				$target_path = $this->_utility->cleanFileName(basename($email[0] . time() . '-PROFILE-' . $_FILES['profile_image']['name']));
				
				$complete_path = $path . $target_path;
				
				$status = move_uploaded_file($_FILES['profile_image']['tmp_name'], $complete_path);
				
				if ($status == 1) {
					$status = $this->user->writeProfileImage($user['id'], $target_path);
				}
			} else {
				$this->load->model('vendor');
				$vendor = $this->vendor->getRecord($_POST['vendor_id']);
				
				$email = explode('@', $vendor['email']);
				
				$this->load->model('_utility');
				
				$path = "./uploads/clients/" . $company_info['db_name'] . '/vendors/' . $vendor['file_name'] . '/';
				
				$target_path = $this->_utility->cleanFileName(basename($email[0] . time() . '-VENDOR-' . $_FILES['profile_image']['name']));
				
				$complete_path = $path . $target_path;
				
				$status = move_uploaded_file($_FILES['profile_image']['tmp_name'], $complete_path);
				
				if ($status == 1) {
					$status = $this->vendor->writeProfileImage($vendor['id'], $target_path);
				}
			}
		} else {
			$status = 'File must be smaller the 2MB.';
		}

		print $status;
		exit;
	}

}