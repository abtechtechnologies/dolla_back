<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}

	public function index()
	{		
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->page_data['loader'] = $this->_loader->load($this->page_data);
	}
	
	public function forgotPassword()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->page_data['loader'] = $this->_loader->load($this->page_data);
	}
	
	public function resetPasswordAction () {
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$email = $_POST['email'];
	
		//Check if input email exists in database
		$this->load->model('user');
		$user = $this->user->getUserByEmail($email);
	
		//User with that email found
		if (count($user) > 0) {
			$this->load->model('admin');
			$key = $this->admin->randomString();
	
			$this->load->model('maillink');
			$status = $this->maillink->writeData($user['id'], $key);
				
			if ($status == 1) {	
				$this->load->model('companyinfo');
				$company_info = $this->companyinfo->getRecord();
				
				$this->load->library('email');
	
				$this->email->set_mailtype("html");
	
				$this->email->from('info@' . $company_info['site']);
	
				$this->email->to(trim($user['email']));
				//$this->email->cc('another@another-example.com');
				//$this->email->bcc('them@their-example.com');
	
				$this->email->subject($company_info['site'] . ' application');
	
				$data = array(
					'user' => $user,
					'key' => $key,
					'site' => $company_info['site']
				);
	
				$body = $this->load->view('email/forgot-password.phtml', $data, TRUE);
				$this->email->message($body);
	
				$this->email->send();
	
				$status = $this->email->print_debugger();
	
				if (trim(strip_tags($status)) == 0) {
					$status = 1;
				}else {
					$status = strip_tags($status);
				}
			}
		} else {
			$status = 'That email is not found in our system. Please try again.';
		}
	
		print $status;
		exit;
	}
	
	public function reset()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->page_data['page'] = 'sign-up/member';
		$this->page_data['error'] = false;
	
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
	
		$this->page_data['key'] = $this->input->get('key', false);
		$this->page_data['user_id'] = $this->input->get('user_id', false);
	
		if ($this->page_data['key'] != false && $this->page_data['user_id'] != false) {
			$this->load->model('maillink');
			$status = $this->maillink->unlock($this->page_data['user_id'], $this->page_data['key']);
				
			if ($status != 1) {
				redirect('http://' . $company['site']);
			}
		} else {
			redirect('http://' . $company['site']);
		}
	
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function admin()
	{	    
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->page_data['page'] = 'admin/login';
		$this->page_data['error'] = $this->input->get('error');
	
		$this->load->view($this->page_data['page'] . '.phtml', $this->page_data);
	}
	
	public function login()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('user');
		$users = $this->user->getUsers();
		$user_id = 0;
	
		foreach ($users as $i => $data) {
			if (strtolower($data['email']) == strtolower($this->input->post('email'))) {
				if ($data['password'] == $this->input->post('password')) {
					$user_id = $data['id'];
					break;
				}
			} else {
				$user_id = 0;
			}
		}
	
		$this->load->helper('url');
		
		if ($user_id > 0) {
			$user = $this->user->getRecord($user_id);
			
			if ($user['active'] == 0) {
				print 'You must wait for your account to be activated.';
				exit;
			}
			
			$_SESSION['user_id'] = $user_id;
	
			print $user_id;
		} else {
			print 'Login failed, please check your info and try again';
		}
	}
	
	public function loginAdmin()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('user');
		$users = $this->user->getUsers();
		$found = false;
		
		foreach ($users as $id => $data) {
			if (strtolower($data['login']) == strtolower($this->input->get('login'))) {

				//if ($data['password'] == $this->input->get('password')) {
				if (hash_equals($data['password'], crypt($this->input->get('password'), $data['password']))) {
					$found = true;
					$user_id = $data['id'];
					break;
				}
			} else {
				$user_id = 0;
			}
		}
		
		$this->load->model('companyinfo');
		$company_info = $this->companyinfo->getRecord();
	
		if ($found == true) {
			$_SESSION['user_id'] = $user_id;
			$_SESSION['admin_user_id'] = $user_id;
	
			redirect('http://' . $company_info['site'] . '/admin');
		} else {
			redirect('http://' . $company_info['site'] . '/login/admin?error=true');
		}
	}
	
	//Required: $_POST['email'], $_POST['unlock']
	public function loginAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$email = $_POST['email'];
		$unlock = $_POST['unlock'];
		
		$status = 1;
		
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->load->model('user');
			$match = $this->user->getUserByEmail($email);
			
			if (count($match) > 0) {
				if (strtolower($match['password']) == strtolower($unlock) || hash_equals($match['password'], crypt($unlock, $match['password']))) {
					//User found
					$_SESSION['user_id'] = $match['id'];
					$_SESSION['vendor_id'] = 0;
				}
			} else {
				$this->load->model('vendor');
				$match = $this->vendor->getRecordByEmail($email);
				
				if (count($match) > 0) {
					//Vendor found
					if (strtolower($match['password']) == strtolower($unlock) || hash_equals($match['password'], crypt($unlock, $match['password']))) {
						//Password Match
						$_SESSION['user_id'] = 0;
						$_SESSION['vendor_id'] = $match['id'];
					} else {
						$status = 'Invalid login credentials';
					}
				} else {
					$status = 'Email address not found';
				}
			}
		} else {
			$status = 'That is not a properly formatted email address.';
		}
		
		print $status;
		exit;
	}
	
	//Required: $_POST['email'], $_POST['unlock']
	public function loginVendorAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$email = $_POST['email'];
		$unlock = $_POST['unlock'];
		
		$status = 1;
		
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->load->model('vendor');
			$match = $this->vendor->getRecordByEmail($email);
			
			if (count($match) > 0) {
				//Vendor found
				if (strtolower($match['email']) == strtolower($email) || hash_equals($match['password'], crypt($unlock, $match['password']))) {
					//Password Match
					$_SESSION['user_id'] = 0;
					$_SESSION['vendor_id'] = $match['id'];
				} else {
					$status = 'Invalid login credentials.';
				}
			} else {
				$status = 'Email address not found.';
			}
		} else {
			$status = 'That is not a properly formatted email address.';
		}
		
		print $status;
		exit;
	}
	
	public function checkEmail()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('user');
		$emails = $this->user->getEmails();
		
		$status = 1;
		
		if (in_array($_POST['email'], $emails)) {
			$status = 'That email is already in our database, please use another';
		}
		
		print $status;
	}
	
	public function logout()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$_SESSION['user_id'] = 0;
		$_SESSION['vendor_id'] = 0;
		
		$this->load->model('companyinfo');
		$company_info = $this->companyinfo->getRecord();
		
		redirect('http://' . $company_info['site']);
	}
	
	public function createUser()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//@todo
		if ($this->input->get('content_id') > 0) {
			$data = array(
				'content_id' => $this->input->get('content_id'),
				'title' => $this->input->get('title'),
				'attachment_file_name' => $this->input->get('attachment'),
				'content_text' => $this->input->get('content_text'),
			);
		} else {
			$data = array(
				'title' => $this->input->get('title'),
				'attachment_file_name' => $this->input->get('attachment'),
				'content_text' => $this->input->get('content_text'),
			);
		}
	
		$this->load->model('content');
		$status = $this->content->writeData($data);
	
		print $status;
	}

	public function signIn()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->page_data['page'] = 'login/sign-in';
		
		$this->page_data['email'] = $this->input->get('email', '');
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->page_data['loader'] = $this->_loader->load($this->page_data);
	}
	
	public function newUser()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$password = $this->input->post('password', false);
		$key = $this->input->post('key', false);
		
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		if ($password == false || $key == false) {
			redirect('http://'. $company['site']);
		}

		$this->load->model('user');
		$status = $this->user->writePassword($password, $this->input->get('user_id'));

		$this->load->model('maillink');
		
		$mail_link = array();
		$mail_link = $this->maillink->getRecord($key);

		if (count($mail_link) > 1) {
			$status = $this->maillink->delete($mail_link['id']);
		} else {
			$status = 'There was an error processing this request';
		}

		return $status;
	}
	
	public function submitNewPasswordAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$password = $_POST['password'];
		$key = $_POST['key'];
	
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
	
		if ($password == '' || $key == '') {
			redirect('http://'. $company['site']);
		}
	
		$this->load->model('user');
		$status = $this->user->writePassword($password, $_POST['user_id']);
	
		$this->load->model('maillink');
	
		$mail_link = array();
		$mail_link = $this->maillink->getRecord($key);
	
		if (count($mail_link) > 1) {
			$status = $this->maillink->delete($mail_link['id']);
		} else {
			$status = 'There was an error processing this request';
		}
	
		return $status;
	}
	
	public function confirmRegister()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->page_data['page'] = 'login/confirm-register';
	
		$this->page_data['email'] = $this->input->get('email', '');
		$this->page_data['email'] = $this->input->get('email', '');
		$this->page_data['email'] = $this->input->get('email', '');
		$this->page_data['email'] = $this->input->get('email', '');
	
		//Init functions and page load
		$this->load->model('_loader');
		$this->page_data['loader'] = $this->_loader->load($this->page_data);
	}
}