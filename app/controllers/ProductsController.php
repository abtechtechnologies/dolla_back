<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductsController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
	}

	public function index()
	{
		if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0) {
			redirect('http://flowersonflowers.com/sign-up?member=false');
		}
		
		$page['page'] = 'products/index';

		//Init functions and page load
		$this->load->model('_loader');
		$page['loader'] = $this->_loader->load($page);
	}

}