<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UploaderController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function uploadApplication()
	{
		//*****IMPORTANT*****
		$dev = false;
		
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('companyinfo');
		$company_info = $this->companyinfo->getRecord();
		
		$this->load->model('_utility');
		
		$path = "uploads/applications/";
				
		$target_path1 = $this->_utility->cleanFileName(basename(ucfirst($_POST['first_name']) . ' ' . ucfirst($_POST['last_name']) . '-LICENSE-' . $_FILES['uploadedfile1']['name']));

		$target_path1 = $path . $target_path1;

		if(move_uploaded_file($_FILES['uploadedfile1']['tmp_name'], $target_path1)) {
			$status = 1;
			
			$target_path2 = $this->_utility->cleanFileName(basename(ucfirst($_POST['first_name']) . ' ' . ucfirst($_POST['last_name']) . '-REC-' . $_FILES['uploadedfile2']['name']));
			
			$target_path2 = $path . $target_path2;
		
			if(move_uploaded_file($_FILES['uploadedfile2']['tmp_name'], $target_path2)) {
				$status = 1;
			} else {
				$status = 'There was an error uploading your rec. Please check that your file is working properly.';
			}
		} else {
			$status = 'There was an error uploading your ID. Please check that your file is working properly.';
		}
		
		$this->load->helper('url');
		
		switch ($_POST['membership']) {
			case 1:
				$due = 180;
				break;
			case 2:
				$due = 375;
				break;
			case 3:
				$due = 650;
				break;
			default:
				$due = 0;
		}
		
		if ($this->input->post('referral2') == '') {
			$refer = $_POST['referral'];
		} else {
			$refer = $_POST['referral2'];
		}
		
		$this->load->model('user');
		$status = $this->user->writeData(
			0,
			$_POST['first_name'],
			$_POST['last_name'],
			$_POST['email'],
			2,
			$_POST['phone'],
			'',
			$_POST['address'],
			$_POST['city'],
			$_POST['zip'],
			$_POST['membership'],
			intval($due),
			$refer,
			$_POST['discover'],
			$target_path1,
			$target_path2,
			$_POST['referral_code']
		);
		
		if ($status == 1) {			
			$this->load->model('admin');
			$key = $this->admin->randomString();
			
			$this->load->model('maillink');
			$status = $this->maillink->writeData(1, $key);
			
			$this->load->library('email');
			$this->email->set_mailtype("html");
			
			$this->email->from('reporter@' . $company_info['site'], $company_info['name'] . ' Reporter');
			
			$this->email->to('apogeereports@gmail.com');
			
			if ($dev == true) {
				$recip = array(
					'apogeereports@gmail.com',
					'josephcharlesdoom@gmail.com'
				);
			} else {
				$recip = array(
					'apogeereports@gmail.com',
					'freetreeorganics@gmail.com'
				);
			}
			
			$this->email->bcc($recip);
			
			$this->email->subject('Application Recieved - ' . $company_info['site']);
			
			$this->email->attach($target_path1);
			$this->email->attach($target_path2);
			
			$data = array(
				'name' => $this->input->post('first_name') . ' ' . $this->input->post('last_name'),
				'address' => $this->input->post('address'),
				'city' => $this->input->post('city'),
				'zip' => $this->input->post('zip'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'discover' => $this->input->post('discover'),
				'referral' => $this->input->post('referral'),
				'site' => $company_info['site'],
				'key' => $key,
				'status' => $status,
				'site' => $company_info['site']
			);
			
			$body = $this->load->view('email/application-received2.phtml', $data, TRUE);
			$this->email->message($body);
			
			$this->email->send();
			
			$status = $this->email->print_debugger();
			
			if (trim(strip_tags($status)) == 0) {
				$status = 1;
			}
			
			if ($status == 1) {
				if ($due > 0) {
					$user = $this->user->getRecent();
					
					$this->load->model('store');
					$status = $this->store->purchaseMembership($user['id'], $user['membership']);
				}
				
				$this->email->clear(true);
				
				$this->load->library('email');
				$this->email->set_mailtype("html");
				
				$this->email->from('reporter@' . $company_info['site'], $company_info['name'] . ' Reporter');
				
				$this->email->to('apogeereports@gmail.com');
				
				if ($dev == true) {
					$recip = array(
						'apogeereports@gmail.com',
						'josephcharlesdoom@gmail.com'
					);
				} else {
					$recip = array(
						'apogeereports@gmail.com',
						$_POST['email']
					);
				}
				
				$this->email->bcc($recip);
				
				$this->email->subject('Application Recieved! - ' . $company_info['site']);
				
				$data = array(
					'name' => $_POST['first_name'],
					'site' => $company_info['site']
				);
				
				$body = $this->load->view('email/member-signup.phtml', $data, TRUE);
				$this->email->message($body);
				
				$this->email->send();
				
				$status = $this->email->print_debugger();
				
				if (trim(strip_tags($status)) == 0) {
					$status = 1;
				}
			}
	
			if ($status == 1) {
				redirect('http://' . $company_info['site'] . '/home?signup=true');
			} else {
				redirect('http://' . $company_info['site'] . '/join?error=' . $status);
			}
			
		} else {
			//THERE WAS AN ERROR UPLOADING A FILE
			$this->load->library('email');
			$this->email->set_mailtype("html");
			
			$this->email->from('reporter@' . $company_info['site'], $company_info['name'] . ' Reporter');
			
			$this->email->to('apogeereports@gmail.com');
			
			if ($dev == true) {
				$recip = array(
					'apogeereports@gmail.com',
					'josephcharlesdoom@gmail.com'
				);
			} else {
				$recip = array(
					'apogeereports@gmail.com',
					'freetreeorganics@gmail.com'
				);
			}
			
			$this->email->bcc($recip);
			
			$this->email->subject('Application Submission Error - ' . $company_info['site']);
			
			$data = array(
				'error' => $status,
				'name' => $this->input->post('first_name') . ' ' . $this->input->post('last_name'),
				'address' => $this->input->post('address'),
				'city' => $this->input->post('city'),
				'zip' => $this->input->post('zip'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'discover' => $this->input->post('discover'),
				'referral' => $this->input->post('referral'),
				'key' => $key,
				'site' => $company_info['site']
			);
			
			$body = $this->load->view('email/application-fail.phtml', $data, TRUE);
			$this->email->message($body);
			
			$this->email->send();
			
			$status = $this->email->print_debugger();
			
			if (trim(strip_tags($status)) == 0) {
				$status = 1;
			}
			
			if ($status == 1) {
				redirect('http://' . $company_info['site'] . '/home?signup=true');
			} else {
				redirect('http://' . $company_info['site'] . '/join?error=' . $status);
			}
		}
	}
}