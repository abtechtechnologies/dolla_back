<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WebController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function sitemap()
	{
		$this->load->view('home/urllist.txt');
	}

	public function index()
	{		
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		$this->page_data['conn'] = $this->page_data['init']['db'];

		$this->page_data['signup'] = $this->input->get('signup');
		
		$this->load->model('activity');
		$this->page_data['activity'] = $this->activity->getRecordsByUser($_SESSION['user_id']);

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function diet()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		$this->page_data['conn'] = $this->page_data['init']['db'];
				
		$this->load->model('_db');
		$this->page_data['goals'] = $this->_db->sql($this->page_data['init']['db'], 'znutrientgoals', $_SESSION['user_id']);
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function blood()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		$this->page_data['conn'] = $this->page_data['init']['db'];
		
		$this->load->model('_db');
		$this->page_data['bp'] = $this->_db->sql($this->page_data['init']['db'], 'zbloodpressure', $_SESSION['user_id']);
		
		$this->load->model('zbloodpressure');
		
// 		$status = $this->zbloodpressure->writeData(
// 			$this->page_data['init']['db'],
// 			0, 
// 			5, 
// 			1, 
// 			1, 
// 			strtotime(date('Y-m-d h:i:s', strtotime( '-31 years'))), 
// 			83, 
// 			122, 
// 			2
// 		);

		$time = time();

		foreach ($this->page_data['bp'] as $i => $data) {
			$this->page_data['bp'][$i]['date'] = date('M d, Y h:i A', $data['ZDATE'] + 978307200);
			
			$this->page_data['bp'][$i]['unix'] = strtotime($this->page_data['bp'][$i]['date']);
		}
		
		$this->page_data['totals'] = array();
		$this->page_data['days'] = array();
				
		//Data for graph
		for ($i = 0; $i < 7; $i++) {
						
			//$first = date('m/01/Y', strtotime( '-' . $i . ' month'));
			$last = date('Y-m-d', strtotime( '-' . $i . ' day'));
			
			$this->page_data['days'][] = date('D', strtotime( '-' . $i . ' day'));

			$date = explode('-', $last);
			
			//Get records for each day
			//first and last same for now
			
			$records = $this->zbloodpressure->getRecordsByDateRange($this->page_data['init']['db'], $_SESSION['user_id'], $last, $last);
			
			$day_sum_dia = 0;
			$day_sum_sys = 0;
			
			foreach ($records as $id => $data) {
				$day_sum_dia += $data['ZDIASTOLIC'];
				$day_sum_sys += $data['ZSYSTOLIC'];
			}
			
			if (count($records) == 0) {
				$this->page_data['totals'][$last]['avg_dia'] = 0;
				$this->page_data['totals'][$last]['avg_sys'] = 0;
			} else {
				$day_avg_dia = $day_sum_dia / count($records);
				$day_avg_sys = $day_sum_sys / count($records);
				
				$this->page_data['totals'][$last] = $records;
				
				$this->page_data['totals'][$last]['avg_dia'] = $day_avg_dia;
				$this->page_data['totals'][$last]['avg_sys'] = $day_avg_sys;
			}
			
		}
		
		$this->page_data['totals'] = array_reverse($this->page_data['totals']);
		$this->page_data['bp'] = array_reverse($this->page_data['bp']);
		$this->page_data['days'] = array_reverse($this->page_data['days']);

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function meds()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		$this->page_data['conn'] = $this->page_data['init']['db'];
		
		$this->load->model('meds');
		$this->page_data['meds'] = $this->meds->getUserRecords($this->page_data['init']['db'], $_SESSION['user_id']);

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function login()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		$this->page_data['conn'] = $this->page_data['init']['db'];

		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function tests()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		$this->page_data['conn'] = $this->page_data['init']['db'];
		
		$this->load->model('labs');
		$this->page_data['labs'] = $this->labs->getUserRecords($this->page_data['init']['db'], $_SESSION['user_id']);
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function steps()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		$this->page_data['conn'] = $this->page_data['init']['db'];
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function test()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		$this->page_data['conn'] = $this->page_data['init']['db'];
		$conn = $this->page_data['init']['db'];
		
		$this->load->model('zbloodpressure');
		$this->load->model('zdatasets');
		$this->load->model('zheartrate');
		$this->load->model('zlabdata');
		$this->load->model('zmedicationdata');
		$this->load->model('zmedtracking');
		$this->load->model('znixfoodaux');
		$this->load->model('znixfooddata');
		$this->load->model('znutrientdatanixid');
		$this->load->model('znutrientgoals');
		$this->load->model('znutrientnamesnixid');
		$this->load->model('zproximatedatanixid');
		$this->load->model('zproximatenamesnixid');
		$this->load->model('zuser');
		
		
		
		for ($z = 0; $z < 10; $z++) {
			
		
		//****************START CREATE NEW USER METHOD*************************
		
		$users = $this->zuser->getRecords($conn);
		$goals = $this->znutrientgoals->getRecords($conn);
		$meds = $this->zmedicationdata->getRecords($conn);
		$lab_data = $this->zlabdata->getRecords($conn);
		$heartrates = $this->zheartrate->getRecords($conn);
		$blood = $this->zbloodpressure->getRecords($conn);

		//****************NUTRIENT GOALS*************************
		
		$user_id_to_set = 0;
		
		$names = array(
			0 => 'Noah',
			1 => 'Liam',
			2 => 'Mason',
			3 => 'Jacob',
			4 => 'William',
			5 => 'Ethan',
			6 => 'James',
			7 => 'Alexander',
			8 => 'Michael',
			9 => 'Benjamin',
			10 => 'Elijah',
			11 => 'Daniel',
			12 => 'Aiden',
			13 => 'Logan',
			14 => 'Matthew',
			15 => 'Lucas',
			16 => 'Jackson',
			17 => 'David',
			18 => 'Oliver',
			19 => 'Jayden',
			20 => 'Joseph',
			21 => 'Gabriel',
			22 => 'Samuel',
			23 => 'Carter',
			24 => 'Anthony',
			25 => 'John',
			26 => 'Dylan',
			27 => 'Luke',
			28 => 'Henry',
			29 => 'Andrew',
			30 => 'Isaac',
			31 => 'Christopher',
			32 => 'Joshua',
			33 => 'Wyatt',
			34 => 'Sebastian',
			35 => 'Owen',
			36 => 'Caleb',
			37 => 'Nathan',
			38 => 'Ryan',
			39 => 'Jack',
			40 => 'Hunter',
			41 => 'Levi',
			42 => 'Christian',
			43 => 'Jaxon',
			44 => 'Julian',
			45 => 'Landon',
			46 => 'Grayson',
			47 => 'Jonathan',
			48 => 'Isaiah',
			49 => 'Charles',
			50 => 'Thomas',
			51 => 'Emma',
			52 => 'Olivia',
			53 => 'Sophia',
			54 => 'Ava',
			55 => 'Isabella',
			56 => 'Mia',
			57 => 'Abigail',
			58 => 'Emily',
			59 => 'Charlotte',
			60 => 'Harper',
			61 => 'Madison',
			62 => 'Amelia',
			63 => 'Elizabeth',
			64 => 'Sofia',
			65 => 'Evelyn',
			66 => 'Avery',
			67 => 'Chloe',
			68 => 'Ella',
			69 => 'Grace',
			70 => 'Victoria',
			71 => 'Aubrey',
			72 => 'Scarlett',
			73 => 'Zoey',
			74 => 'Addison',
			75 => 'Lily',
			76 => 'Lillian',
			77 => 'Natalie',
			78 => 'Hannah',
			79 => 'Aria',
			80 => 'Layla',
			81 => 'Brooklyn',
			82 => 'Alexa',
			83 => 'Zoe',
			84 => 'Penelope',
			85 => 'Riley',
			86 => 'Leah',
			87 => 'Audrey',
			88 => 'Savannah',
			89 => 'Allison',
			90 => 'Samantha',
			91 => 'Nora',
			92 => 'Skylar',
			93 => 'Camila',
			94 => 'Anna',
			95 => 'Paisley',
			96 => 'Ariana',
			97 => 'Ellie',
			98 => 'Aaliyah',
			99 => 'Claire',
			100 => 'Violet',
			101 => 'Jordan',
			102 => 'Brayden',
			103 => 'Nicholas',
			104 => 'Robert',
			105 => 'Angel',
			106 => 'Hudson',
			107 => 'Lincoln',
			108 => 'Evan',
			109 => 'Dominic',
			110 => 'Austin',
			111 => 'Gavin',
			112 => 'Nolan',
			113 => 'Parker',
			114 => 'Adam',
			115 => 'Chase',
			116 => 'Jace',
			117 => 'Ian',
			118 => 'Cooper',
			119 => 'Easton',
			120 => 'Kevin',
			121 => 'Jose',
			122 => 'Tyler',
			123 => 'Brandon',
			124 => 'Asher',
			125 => 'Jaxson',
			126 => 'Mateo',
			127 => 'Jason',
			128 => 'Ayden',
			129 => 'Zachary',
			130 => 'Carson',
			131 => 'Xavier',
			132 => 'Leo',
			133 => 'Ezra',
			134 => 'Bentley',
			135 => 'Sawyer',
			136 => 'Kayden',
			137 => 'Blake',
			138 => 'Nathaniel'
		);
		
		$last_names = array(
			0 => 'Ahart',
			1 => 'Smith',
			2 => 'Johnson',
			3 => 'Williams',
			4 => 'Jones',
			5 => 'Brown',
			6 => 'Davis',
			7 => 'Miller',
			8 => 'Wilson',
			9 => 'Moore',
			10 => 'Taylor',
			11 => 'Anderson',
			12 => 'Thomas',
			13 => 'Jackson',
			14 => 'White',
			15 => 'Harris',
			16 => 'Martin',
			17 => 'Thompson',
			18 => 'Garcia',
			19 => 'Martinez',
			20 => 'Robinson',
			21 => 'Clark',
			22 => 'Rodriguez',
			23 => 'Lewis',
			24 => 'Lee',
			25 => 'Walker',
			26 => 'Hall',
			27 => 'Allen',
			28 => 'Young',
			29 => 'Hernandez',
			30 => 'King',
			31 => 'Wright',
			32 => 'Lopez',
			33 => 'Hill',
			34 => 'Scott',
			35 => 'Green',
			36 => 'Adams',
			37 => 'Baker',
			38 => 'Gonzalez',
			39 => 'Nelson',
			40 => 'Carter',
			41 => 'Mitchell',
			42 => 'Perez',
			43 => 'Roberts',
			44 => 'Turner',
			45 => 'Phillips',
			46 => 'Campbell',
			47 => 'Parker',
			48 => 'Evans',
			49 => 'Edwards',
			50 => 'Collins',
			51 => 'Stewart',
			52 => 'Sanchez',
			53 => 'Morris',
			54 => 'Rogers',
			55 => 'Reed',
			56 => 'Cook',
			57 => 'Morgan',
			58 => 'Bell',
			59 => 'Murphy',
			60 => 'Bailey',
			61 => 'Rivera',
			62 => 'Cooper',
			63 => 'Richardson',
			64 => 'Cox',
			65 => 'Howard',
			66 => 'Ward',
			67 => 'Torres',
			68 => 'Peterson',
			69 => 'Gray',
			70 => 'Ramirez',
			71 => 'James',
			72 => 'Watson',
			73 => 'Brooks',
			74 => 'Kelly',
			75 => 'Sanders',
			76 => 'Price',
			77 => 'Bennett',
			78 => 'Wood',
			79 => 'Barnes',
			80 => 'Ross',
			81 => 'Henderson',
			82 => 'Coleman',
			83 => 'Jenkins',
			84 => 'Perry',
			85 => 'Powell',
			86 => 'Long',
			87 => 'Patterson',
			88 => 'Hughes',
			89 => 'Flores',
			90 => 'Washington',
			91 => 'Butler',
			92 => 'Simmons',
			93 => 'Foster',
			94 => 'Gonzales',
			95 => 'Bryant',
			96 => 'Alexander',
			97 => 'Russell',
			98 => 'Griffin',
			99 => 'Diaz',
			100 => 'Hayes',
		);
		
		foreach ($users as $i => $data) {
			if ($user_id_to_set < $data['Z_PK']) {
				$user_id_to_set = $data['Z_PK'];
			}
		}
		
		$user_id_to_set += 1;
		
		$rand2 = rand(0, 138);
		$rand1 = rand(0, 100);
		
		//$conn, $id, $Z_PK, $ZFIRSTNAME, $ZLASTNAME, $ZEMAIL, $ZPASSWORD
		$status = $this->zuser->writeData(
			$conn,
			0,
			$user_id_to_set,
			ucfirst($names[$rand1]),
			ucfirst($last_names[$rand2]),
			strtolower(substr($names[$rand1], 0, 1)) . strtolower($last_names[$rand2]) . 'ABTECH@gmail.com',
			'test'
		);
		
		if ($status != 1) {
			print '<pre>';
			print_r('Failed at User');
			print '</pre>';
			exit;
		}
		
		//****************NUTRIENT GOALS*************************
		
		
		$new_index = 0;
		
		foreach ($goals as $i => $data) {
			if ($new_index < $data['Z_PK']) {
				$new_index = $data['Z_PK'];
			}
		}
		
		$new_index += 1;
		
		$goal_array = array(
			0 => array(
				'ZCOMMON' => 'Carbohydrate',
				'ZNAME' => 'Carbohydrate',
				'ZUNIT' => 'g',
				'ZVALUE' => rand(250, 350)
			),
			1 => array(
				'ZCOMMON' => 'Protein',
				'ZNAME' => 'Protein',
				'ZUNIT' => 'g',
				'ZVALUE' => rand(50, 115)
			),
			2 => array(
				'ZCOMMON' => 'Calories',
				'ZNAME' => 'Energy',
				'ZUNIT' => 'Cals',
				'ZVALUE' => rand(1500, 3200)
			),
			3 => array(
				'ZCOMMON' => 'Fiber',
				'ZNAME' => 'Fiber, total dietary',
				'ZUNIT' => 'g',
				'ZVALUE' => rand(15, 32)
			),
			4 => array(
				'ZCOMMON' => 'Sugar',
				'ZNAME' => 'Sugars, total',
				'ZUNIT' => 'g',
				'ZVALUE' => rand(5, 42)
			),
			5 => array(
				'ZCOMMON' => 'Sodium',
				'ZNAME' => 'Sodium, Na',
				'ZUNIT' => 'mg',
				'ZVALUE' => rand(1150, 1400)
			),
			6 => array(
				'ZCOMMON' => 'Fat',
				'ZNAME' => 'Total lipid (fat)',
				'ZUNIT' => 'g',
				'ZVALUE' => rand(48, 89)
			),
			7 => array(
				'ZCOMMON' => 'Cholesterol',
				'ZNAME' => 'Cholesterol',
				'ZUNIT' => 'mg',
				'ZVALUE' => rand(220, 470)
			)
		);
		
		for ($j = 0; $j < 8; $j++) {
			//$conn, $id, $Z_PK, $Z_ENT, $Z_OPT, $ZVALUE, $ZCOMMON, $ZNAME, $ZUNIT, $ZUSERID
			$status = $this->znutrientgoals->writeData(
				$conn,
				0,
				$new_index,
				1,
				1,
				$goal_array[$j]['ZVALUE'],
				$goal_array[$j]['ZCOMMON'],
				$goal_array[$j]['ZNAME'],
				$goal_array[$j]['ZUNIT'],
				$user_id_to_set
			);
			
			$new_index += 1;
		}
		
		if ($status != 1) {
			print '<pre>';
			print_r('Failed at Nutrient Goals');
			print '</pre>';
			exit;
		}
		
		//****************FOOD DATA*************************
		
		$food_aux = $this->znutrientdatanixid->getRecords($conn);
		$new_index = 0;

		
		//****************FOOD DATA*************************
		
		$food_aux = $this->znixfooddata->getRecords($conn);
		$new_index = 0;

		
		//****************MED TRACKING*************************
		
		$med_tracks = $this->zmedtracking->getRecords($conn);
		$new_index = 0;
		
		
		//****************MEDICATION DATA*************************
		
		
		$new_index = 0;
		
		foreach ($meds as $i => $data) {
			if ($new_index < $data['Z_PK']) {
				$new_index = $data['Z_PK'];
			}
		}
		
		$new_index += 1;
		
		$test_array = array(
			0 => 'Acetaminophen',
			1 => 'Adderall',
			2 => 'Alprazolam',
			3 => 'Amitriptyline',
			4 => 'Amlodipine',
			5 => 'Amoxicillin',
			6 => 'Ativan',
			7 => 'Atorvastatin',
			8 => 'Azithromycin',
			9 => 'Ciprofloxacin',
			10 => 'Citalopram',
			11 => 'Clindamycin',
			12 => 'Clonazepam',
			13 => 'Codeine',
			14 => 'Cyclobenzaprine',
			15 => 'Cymbalta',
			16 => 'Doxycycline',
			17 => 'Gabapentin',
			18 => 'Hydrochlorothiazide',
			19 => 'Ibuprofen',
			20 => 'Lexapro',
			21 => 'Lisinopril',
			22 => 'Loratadine',
			23 => 'Lorazepam',
			24 => 'Losartan',
			25 => 'Lyrica',
			26 => 'Meloxicam',
			27 => 'Metformin',
			28 => 'Metoprolol',
			29 => 'Naproxen',
			30 => 'Omeprazole',
			31 => 'Oxycodone',
			32 => 'Pantoprazole',
			33 => 'Prednisone',
			34 => 'Tramadol',
			35 => 'Trazodone',
			36 => 'Viagra',
			37 => 'Wellbutrin',
			38 => 'Xanax',
			39 => 'Zoloft'
		);
		
		$comment_array = array(
			0 => 'From tracking',
			1 => '',
			2 => 'Test comment'
		);
		
		$num = rand(4, 16);
		
		for ($j = 0; $j < $num; $j++) {
			$rand_day = rand(1, 14);
			
			$rand1 = rand(82, 6000);
			$rand2 = rand(0, 39);
			$rand3 = round((rand(25, 150)+5/2)/5)*5;
			$rand4 = rand(0, 3);
			$rand5 = rand(0, 2);
			
			//$conn, $id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZAMOUNT, $ZNDOSES, $ZVAL1, $ZBRAND, $ZCOMMENT, $ZDOSAGEINFO, $ZFDAID, $ZGENERIC, $ZMANUFACTURER, $ZSTR1, $ZUNITS, $ZUSERID
			$status = $this->zmedicationdata->writeData(
				$conn,
				0,
				$new_index,
				1,
				1,
				strtotime(date('Y-m-d h:i:s', strtotime( '-31 years -' . $rand_day . ' days'))),
				
				//Amount
				$rand3,
				$rand4,
				0,
				'',
				$comment_array[$rand5],
				$rand3 . ' MG Tablet',
				'',
				$test_array[$rand2],
				'',
				'',
				'mg', 
					$user_id_to_set
			);
			
			if ($status != 1) {
				print '<pre>';
				print_r('Failed at Medication Data');
				print '</pre>';
				exit;
			}
			
			$new_index += 1;
		}
		
		
	
		
		//****************LAB DATA*************************
		
		$new_index = 0;
		
		foreach ($lab_data as $i => $data) {
			if ($new_index < $data['Z_PK']) {
				$new_index = $data['Z_PK'];
			}
		}
		
		$new_index += 1;
		
		$test_array = array(
			0 => 'Abdominoplasty (Tummy Tuck ( Abdominoplasty))',
			1 => 'Ablation Therapy for Arrhythmias',
			2 => 'Ablation, Endometrial (Endometrial Ablation)',
			3 => 'Ablation, Uterus (Endometrial Ablation)',
			4 => 'Abnormal Liver Enzymes (Liver Blood Tests)',
			5 => 'Absorbent Products Incontinence (Urinary Incontinence Products for Men)',
			6 => 'Abstinence Method of Birth Control (Natural Methods of Birth Control)',
			7 => 'Acupuncture',
			8 => 'Adenoidectomy Surgical Instructions',
			9 => 'Adenosine (Exercise Stress Test)',
			10 => 'Adenosine Stress Test For Heart Disease (Coronary Artery Disease Screening Tests (CAD))',
			11 => 'AFP Blood Test (Alpha-fetoprotein Blood Test)',
			12 => 'Alexander Technique for Childbirth (Childbirth Class Options)',
			13 => 'ALK (Keratoplasty Eye Surgery (ALK))',
			14 => 'Allergy Shots',
			15 => 'Allergy, Skin Test (Skin Test For Allergy)',
			16 => 'Alpha-fetoprotein Blood Test',
			17 => 'ALT Test (Liver Blood Tests)',
			18 => 'AMA (Antimitochondrial Antibodies)',
			19 => 'Amino Acid, Homocysteine (Homocysteine)',
			20 => 'Amniocentesis',
			21 => 'Amniotic Fluid (Amniocentesis)',
			22 => 'ANA (Antinuclear Antibody)',
			23 => 'Analysis of Urine (Urinalysis)',
			24 => 'Angiogram Of Heart (Coronary Angiogram)',
			25 => 'Angioplasty (Coronary Angioplasty)',
			26 => 'Annulus Support (Heart Valve Disease Treatment)',
			27 => 'Anti-CCP (Citrulline Antibody)',
			28 => 'Anti-citrulline Antibody (Citrulline Antibody)',
			29 => 'Anti-cyclic Citrullinated Peptide Antibody (Citrulline Antibody)',
			30 => 'Anti-Reflux Surgery (Fundoplication)',
			31 => 'Antimicrosomal Antibody Test (Thyroid Peroxidase Test)',
			32 => 'Antimitochondrial Antibodies',
			33 => 'Antinuclear Antibody',
			34 => 'Antithyroid Microsomal Antibody Test (Thyroid Peroxidase Test)',
			35 => 'Antro-duodenal Motility Study',
			36 => 'Aortic Heart Valve Replacement (Heart Valve Disease Treatment)',
			37 => 'Apgar Score',
			38 => 'Appendectomy',
			39 => 'Arrhythmia Treatment (Ablation Therapy for Arrhythmias)',
			40 => 'Arthritis Physical and Occupational Therapy',
			41 => 'Arthrocentesis (Joint Aspiration)',
			42 => 'Arthroplasty (Joint Replacement Surgery Of The Hand)',
			43 => 'Arthroscopy',
			44 => 'Artificial Kidney (Hemodialysis)',
			45 => 'Aspiration, Joint (Joint Aspiration)',
			46 => 'AST Test (Liver Blood Tests)',
			47 => 'Astigmatic Keratotomy Eye Surgery',
			48 => 'Auditory Brainstem Response (Newborn Infant Hearing Screening)',
			49 => 'Augmentation, Lip (Lip Augmentation)',
			50 => 'Autism Screening and Diagnosis',
			51 => 'Autopsy'
		);
		
		$num = rand(4, 16);
		
		for ($j = 0; $j < $num; $j++) {
			$rand_day = rand(1, 14);
			
			$rand1 = rand(82, 6000);
			$rand2 = rand(0, 51);
			
			//$conn, $id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZVALUE, $ZCOMMENT, $ZNAME, $ZUSERID
			$status = $this->zlabdata->writeData(
				$conn,
				0,
				$new_index,
				1,
				1,
				strtotime(date('Y-m-d h:i:s', strtotime( '-31 years -' . $rand_day . ' days'))),
				$rand1,
				'This is a comment',
				$test_array[$rand2],
				$user_id_to_set
			);
			
			if ($status != 1) {
				print '<pre>';
				print_r('Failed at Lab Data');
				print '</pre>';
				exit;
			}
			
			$new_index += 1;
		}

		//****************HEART RATE*************************
		
		$new_index = 0;
		
		foreach ($heartrates as $i => $data) {
			if ($new_index < $data['Z_PK']) {
				$new_index = $data['Z_PK'];
			}
		}
		
		$new_index += 1;
		
		$num = rand(4, 16);
		
		for ($j = 0; $j < $num; $j++) {
			$rand_day = rand(1, 14);
			
			$rand1 = rand(82, 172);
			
			//$conn, $id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZRATE, $ZCOMMENT, $ZUSERID
			$status = $this->zheartrate->writeData(
				$conn,
				0,
				$new_index,
				1,
				1,
				strtotime(date('Y-m-d h:i:s', strtotime( '-31 years -' . $rand_day . ' days'))),
				$rand1,
				'This is a comment',
				$user_id_to_set
			);
			
			if ($status != 1) {
				print '<pre>';
				print_r('Failed at Heart Rate');
				print '</pre>';
				exit;
			}
			
			$new_index += 1;
		}

		//****************BLOOD PRESSURE*************************
		
		$new_index = 0;

		foreach ($blood as $i => $data) {
			if ($new_index < $data['Z_PK']) {
				$new_index = $data['Z_PK'];
			}
		}
		
		$new_index += 1;
		
		$num = rand(4, 16);

		for ($j = 0; $j < $num; $j++) {
			$rand_day = rand(1, 14);
			
			$rand1 = rand(65, 95);
			$rand2 = rand(105, 135);
			
			//$conn, $id, $Z_PK, $Z_ENT, $Z_OPT, $ZDATE, $ZDIASTOLIC, $ZSYSTOLIC, $ZUSERID
			$status = $this->zbloodpressure->writeData(
				$conn,
				0,
				$new_index,
				1,
				1,
				strtotime(date('Y-m-d h:i:s', strtotime( '-31 years -' . $rand_day . ' days'))),
				$rand1,
				$rand2,
				$user_id_to_set
			);
			
			if ($status != 1) {
				print '<pre>';
				print_r('Failed at Blood Pressure');
				print '</pre>';
				exit;
			}
			
			$new_index += 1;
		}
		
		}
		
		//Init functions and page load
		$this->load->model('_loader2');
		$this->_loader2->load($this->page_data);
	}
	
	public function location()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'home/location';
		
		//Init functions and page load
		$this->load->model('_loader');
		$page['loader'] = $this->_loader->load($page);
	}
	
	public function elements()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'home/elements';
	
		//Init functions and page load
		$this->load->model('_loader');
		$page['loader'] = $this->_loader->load($page);
	}
	
	public function saleTest()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'home/sale-test';
		
		 require 'lib/paytrace/vendor/autoload.php';
		
		$client = new GuzzleHttp\Client();
		
		// DRY
		$auth_server = 'https://api.paytrace.com';
		
		// send a request to the authentication server
		// note: normally, you'd store the username/password in a more secure fashion!
		$res = $client->post($auth_server . "/oauth/token", [
			'body' => [
				'grant_type' => 'password', 
				'username' => 'JoeAhart', 
				'password' => 'Tranzq123!'
			]
		]);
		
		// grab the results (as JSON)
		$json = $res->json();
		
		// get the access token
		$token = $json['access_token'];
		
		// create a fake sale transaction
		$sale_data = array(
			"amount" => 19.99, 
			"credit_card" => array(
				"number" => "4111111111111111",
				"expiration_month" => "12",
				"expiration_year" => "2020"
			),
			"billing_address" => array(
				"name" => "Steve Smith", 
				"street_address" => "8320 E. West St.",
				"city" => "Spokane",
				"state" => "WA", 
				"zip" => "85284"
			)
		);
		
		try {
			// post the transaction to hermes
			$res = $client->post($auth_server . "/v1/transactions/sale/keyed", [
				'headers' => [  'Authorization' => "Bearer " . $token],
				'json' => $sale_data
			]);
		} catch (Exception $e) {
			//@TODO
				print_r('!!! - ' . $e->getMessage());
				exit;
		}
		
		$response = $res->json();
		
		// dump the json results
		$result = var_export($res->json());
			
		//@TODO
			print '<pre>';
			print_r($result);
			print '</pre>';
			exit;
	}

}