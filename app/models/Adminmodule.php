<?php
class Adminmodule extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getRecentId()
	{
		$sql = 'SELECT * FROM admin_module WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = 0;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM admin_module WHERE id = ' . $id . ' AND deleted = 0;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM admin_module WHERE deleted = 0 ORDER BY id;';

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}

	public function writeData($data)
	{
		$sql =
			"INSERT INTO `admin_module`
			(
				`created_by`, 
				`user_id`, 
                `product_id`, 
                `rating`, 
                `description`
			) VALUES (
				'" . $data['user_id'] . "', 
				'" . $data['user_id'] . "',
				'" . $data['product_id'] . "',
				'" . $data['rating'] . "',
				'" . $data['description'] . "'
			);";
		
		return $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `admin_module` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}

}