<?php
class Cart extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM cart WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecentId()
	{
		$sql = 'SELECT * FROM cart WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = 0;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM cart WHERE deleted = 0;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM cart WHERE user_id = ' . $id . ' AND deleted = 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function writeData($user_id, $deal, $quantity = 1)
	{
		if (isset($_SESSION['user_id'])) {
			$sql =
				"INSERT INTO `cart`
				(
					`created_by`,
					`user_id`,
					`deal_id`,
					`quantity`
				) VALUES (
					'" . $_SESSION['user_id'] . "',
					'" . $user_id . "',
					'" . $deal['id'] . "',
					'" . $quantity . "'
				);";
			
			return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		} else {
			return 'Your session has ended. Please log in again.';
		}
	}
	
	public function increaseQuantity($id)
	{
		$sql = 'SELECT * FROM cart WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$cart = $row;
		}
		
		$new_quantity = $cart['quantity'] + 1;
		
		$sql = 'UPDATE `cart` SET quantity = ' . $new_quantity . ' WHERE id = ' . $id;
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function decreaseQuantity($id)
	{
		$sql = 'SELECT * FROM cart WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$cart = $row;
		}
		
		$new_quantity = $cart['quantity'] - 1;
		
		$sql = 'UPDATE `cart` SET quantity = ' . $new_quantity . ' WHERE id = ' . $id;
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($cart_id)
	{
		$sql = 'UPDATE `cart` SET deleted = 1 WHERE id = ' . $cart_id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
	
	public function emptyCart($user_id)
	{
		$sql = 'UPDATE `cart` SET deleted = 1 WHERE user_id = ' . $user_id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}