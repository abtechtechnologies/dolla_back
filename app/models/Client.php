<?php
class Client extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getMostRecent()
	{
		$sql = 'SELECT * FROM client WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM client WHERE id = ' . $id . ' AND deleted = 0;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecordByDbName($db_name)
	{
		$sql = 'SELECT * FROM client WHERE db_name = "' . $db_name . '" AND deleted = 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM client WHERE deleted = 0;';

		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM client WHERE 1;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `client` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
	
}