<?php
class Companyinfo extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
		
	}
	
	public function getRecord($client_id = -1)
	{
		$this->load->model('client');
		
		if ($client_id == -1) {
			if (isset($_SESSION['client_id']) && $_SESSION['client_id'] > 0) {
				$client = $this->client->getRecord($_SESSION['client_id']);
			} else {
				$client = $this->client->getRecord(1);
			}
		} else {
			$client = $this->client->getRecord($client_id);
		}
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
		
		$sql = 'SELECT * FROM company_info WHERE id = 1 AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function writeData($data)
	{
		if (isset($data['content_id'])) {
			$sql =
			"UPDATE `content` SET
				modified_by = " . $_SESSION['user_id'] . ",
				title = '" . str_replace("'", "\'", $data['title']) . "',
				content_text = '" . str_replace("'", "\'", $data['content_text']) . "',
				target_date = '" . str_replace("'", "\'", $data['target_date']) . "',
				attachment_file_name = '" . str_replace("'", "\'", $data['attachment']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['content_id']) . ";";
		} else {
			$sql =
			"INSERT INTO `content`
			(
				`created_by`,
				`title`,
				`content_text`,
				`attachment_file_name`,
				`target_date`,
				`content_type_id`
			) VALUES ('
				" . $_SESSION['user_id'] . "', '
				" . str_replace("'", "\'", trim($data['title'])) . "', '
				" . str_replace("'", "\'", $data['content_text']) . "', '
				" . str_replace("'", "\'", trim($data['attachment_file_name'])) . "', '
				" . str_replace("'", "\'", $data['target_date']) . "', '
				" . str_replace("'", "\'", $data['content_type_id']) . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function changePrimaryHolder($user_id)
	{
		$sql =
		"UPDATE `company_info` SET
			primary_inventory_user_id = " . $user_id . "
		WHERE
			id = 1;";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function writeNormalSale($value)
	{
		$sql =
		"UPDATE `company_info` SET
			normal_sale = " . $value . "
		WHERE
			id = 1;";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function writeDefaultFilter($value)
	{
		$sql =
		"UPDATE `company_info` SET
			default_sale_filter = " . $value . "
		WHERE
			id = 1;";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function switchAllowNegative($source)
	{
		$sql =
		"UPDATE `company_info` SET
			allow_negative = " . $source . "
		WHERE
			id = 1;";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
}