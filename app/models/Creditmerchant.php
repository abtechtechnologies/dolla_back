<?php
class Creditmerchant extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getMostRecent()
	{
		$sql = 'SELECT * FROM credit_merchant WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM credit_merchant WHERE id = ' . $id . ' AND deleted = 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM credit_merchant WHERE deleted = 0 ORDER BY id;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
			$return[$row['id']]['total_sales'] = 0;
		}
		
		return $return;
	}
	
	public function getRecordsAlt()
	{
	    $sql = 'SELECT * FROM credit_merchant WHERE deleted = 0 ORDER BY id;';
	
	    $db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {

	        $return[$row['product_id']][$row['id']] = $row;
	    }
	
	    return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM credit_merchant WHERE user_id = ' . $id . ' AND deleted = 0 ORDER BY id;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByProductId($product_id)
	{
	    $sql = 'SELECT * FROM credit_merchant WHERE product_id = ' . $product_id . ' AND deleted = 0 ORDER BY id;';
	
	    $db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {
	        $return[$row['id']] = $row;
	    }
	
	    return $return;
	}

	public function writeData($id, $name, $rate, $rep_name, $phone, $email)
	{
		if ($id > 0) {
			$sql =
			"UPDATE `credit_merchant` SET
				modified_by = " . $_SESSION['user_id'] . ",
				name = '" . str_replace("'", "\'", $name) . "',
				rate = '" . str_replace("'", "\'", $rate) . "',
				rep_name = '" . str_replace("'", "\'", $rep_name) . "',
				phone = '" . str_replace("'", "\'", $phone) . "',
				email = '" . str_replace("'", "\'", $email) . "'
			WHERE
				id = " . str_replace("'", "\'", $id) . ";";
		} else {
			$sql =
			"INSERT INTO `credit_merchant`
			(
				`created_by`,
				`name`,
				`rate`,
				`rep_name`,
				`phone`,
				`email`
			) VALUES (
				'" . $_SESSION['admin_user_id'] . "',
				'" . $name . "',
				'" . $rate . "',
				'" . $rep_name . "',
				'" . $phone . "',
				'" . $email . "'
			);";
		}
		
		
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `credit_merchant` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}

}