<?php
class Deal extends CI_Model {

	public function __construct()
	{
		$this->load->database();

		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM deal WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	
	public function getRecords($include_blank = false)
	{				
		$sql = 'SELECT * FROM deal WHERE deleted = 0 ORDER BY id;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		if ($include_blank == true) {
			$return[0] = array();
			$return[0]['name'] = 'None';
			
		}
				
		foreach ($db->result_array() as $row) {
			
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByVendorId($vendor_id)
	{
		$sql = 'SELECT * FROM deal WHERE vendor_id = ' . $vendor_id . ' AND deleted = 0 ORDER BY id;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();

		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getFeatured()
	{
		$sql = 'SELECT * FROM deal WHERE featured > 0 AND deleted = 0 ORDER BY id;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM deal WHERE 1;';
	
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecentdealId()
	{
		$sql = 'SELECT * FROM deal WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getRecordsByDealType()
	{
		$sql = 'SELECT * FROM deal WHERE deleted = 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			
			if (!isset($return[$row['deal_type_id']])) {
				$return[$row['deal_type_id']] = array();
			}
			
			$return[$row['deal_type_id']][$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByDealTypeId($deal_type_id)
	{
		$sql = 'SELECT * FROM deal WHERE deleted = 0 AND deal_type_id = ' . $deal_type_id . ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getdealsByParentId($deal_id)
	{
		$sql = 'SELECT * FROM deal WHERE deleted = 0 AND parent_id = ' . $deal_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getdealsByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[] = $row['deal_id'];
		}
	
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM deal WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getdealSizes()
	{
		$sql = 'SELECT * FROM deal WHERE deleted = 0 AND parent_id > 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function writeData($deal_id, $vendor_id, $title, $expire, $description, $image, $original_price, $value, $value_type)
	{
		if ($deal_id > 0) {
			$sql =
			"UPDATE `deal` SET
				modified_by = " . $_SESSION['user_id'] . ",
				rep_name = '" . str_replace("'", "\'", trim($data['rep_name'])) . "',
				phone = '" . str_replace("'", "\'", trim($data['phone'])) . "',
				address = '" . str_replace("'", "\'", trim($data['address'])) . "',
				website = '" . str_replace("'", "\'", trim($data['website'])) . "',
				name = '" . str_replace("'", "\'", trim($data['name'])) . "',
				description = '" . str_replace("'", "\'", trim($data['description'])) . "',
				attachment_file_name = '" . str_replace("'", "\'", $data['attachment']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['deal_id']) . ";";
		} else {
			$sql =
			"INSERT INTO `deal`
			(
				`created_by`, 
				`vendor_id`, 
				`title`, 
				`expire`, 
				`description`, 
				`image`, 
				`original_price`, 
				`value`,
				`value_type`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . str_replace("'", "\'", trim($vendor_id)) . "', 
				'" . str_replace("'", "\'", trim($title)) . "', 
				'" . str_replace("'", "\'", trim($expire)) . "',
				'" . str_replace("'", "\'", trim($description)) . "', 
				'" . str_replace("'", "\'", trim($image)) . "', 
				'" . str_replace("'", "\'", trim($original_price)) . "', 
				'" . str_replace("'", "\'", trim($value)) . "', 
				'" . str_replace("'", "\'", trim($value_type)) . "'
			);";
		}
		
		$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `deal` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		if ($status == 1) {
			$sql = 'UPDATE `inventory` SET deleted = 1 WHERE deal_id = ' . $id;
			
			$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		}
	
		return $status;
	}
}