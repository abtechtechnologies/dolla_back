<?php
class Dealrequest extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM deal_request WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM deal_request WHERE deleted = 0 ORDER BY id;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();

		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByString($deal_string)
	{
		$sql = 'SELECT * FROM deal_request WHERE deal_id IN (' . $deal_string . ') AND deleted = 0 ORDER BY id;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM deal_request WHERE 1;';
	
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecentdeal_requestId()
	{
		$sql = 'SELECT * FROM deal_request WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getdeal_requestsBydeal_requestType($deal_request_type_id)
	{
		$sql = 'SELECT * FROM deal_request WHERE deleted = 0 AND parent_id = 0 AND deal_request_type_id = ' . $deal_request_type_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getdeal_requestsByParentId($deal_request_id)
	{
		$sql = 'SELECT * FROM deal_request WHERE deleted = 0 AND parent_id = ' . $deal_request_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getdeal_requestsByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[] = $row['deal_request_id'];
		}
	
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM deal_request WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getdeal_requestSizes()
	{
		$sql = 'SELECT * FROM deal_request WHERE deleted = 0 AND parent_id > 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function writeData($id, $user_id, $deal_id)
	{
		if ($id > 0) {
			$sql =
			"UPDATE `deal_request` SET
				modified_by = " . $_SESSION['user_id'] . ",
				rep_name = '" . str_replace("'", "\'", trim($data['rep_name'])) . "',
				phone = '" . str_replace("'", "\'", trim($data['phone'])) . "',
				address = '" . str_replace("'", "\'", trim($data['address'])) . "',
				website = '" . str_replace("'", "\'", trim($data['website'])) . "',
				name = '" . str_replace("'", "\'", trim($data['name'])) . "',
				description = '" . str_replace("'", "\'", trim($data['description'])) . "',
				attachment_file_name = '" . str_replace("'", "\'", $data['attachment']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['deal_request_id']) . ";";
		} else {
			$sql =
			"INSERT INTO `deal_request`
			(
				`created_by`, 
				`user_id`, 
				`deal_id`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . str_replace("'", "\'", trim($user_id)) . "', 
				'" . str_replace("'", "\'", trim($deal_id)) . "'
			);";
		}
		
		$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `deal_request` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
}