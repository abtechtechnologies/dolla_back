<?php
class Menuitempart extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getMostRecent()
	{
		$sql = 'SELECT * FROM menu_item_part WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM menu_item_part WHERE id = ' . $id . ' AND deleted = 0 AND order_delivered = 0 ;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM menu_item_part WHERE deleted = 0 ORDER BY id;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAlt()
	{
	    $sql = 'SELECT * FROM menu_item_part WHERE deleted = 0 ORDER BY id;';
	
	    $db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {

	        $return[$row['product_id']][$row['id']] = $row;
	    }
	
	    return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM menu_item_part WHERE user_id = ' . $id . ' AND deleted = 0 ORDER BY id;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByMenuItemId($menu_item_id)
	{
		$sql = 'SELECT * FROM menu_item_part WHERE menu_item_id = ' . $menu_item_id . ' AND deleted = 0 ORDER BY id;';
	
	    $db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {
	        $return[$row['id']] = $row;
	    }
	
	    return $return;
	}

	public function writeData($menu_item_part_id, $menu_item_id, $product_id, $units)
	{
		if ($menu_item_part_id == 0) {
			$sql =
			"INSERT INTO `menu_item_part`
			(
				`created_by`,
				`menu_item_id`,
				`product_id`,
				`product_units`
			) VALUES (
				'" . $_SESSION['admin_user_id'] . "',
				'" . $menu_item_id . "',
				'" . $product_id . "',
				'" . $units . "'
			);";
		} else {
			$sql =
			"UPDATE `menu_item_part` SET
				modified_by = " . $_SESSION['admin_user_id'] . ",
				menu_item_id = '" . str_replace("'", "\'", trim($menu_item_id)) . "',
				product_id = '" . str_replace("'", "\'", trim($product_id)) . "',
				product_units = '" . str_replace("'", "\'", trim($units)) . "'
			WHERE
				id = " . str_replace("'", "\'", $menu_item_part_id) . ";";
		}
		
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `menu_item_part` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}

}