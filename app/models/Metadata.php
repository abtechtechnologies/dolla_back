<?php
class Metadata extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecent()
	{
		$sql = 'SELECT * FROM metadata WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecentID()
	{
		$sql = 'SELECT * FROM metadata WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getRecord()
	{
		$sql = 'SELECT * FROM metadata WHERE id = 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM metadata WHERE deleted = 0 ORDER BY id;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function generateCode($metadata_type_id, $user_id)
	{
		$code = '';
		
		$seed = str_split('ABCDEFGHJKLMNPQRSTUVWXYZ23456789');
		
		shuffle($seed); // probably optional since array_is randomized; this may be redundant
		$rand = '';
		foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
		
		$sql =
		"INSERT INTO `metadata`
		(
			`created_by`,
			`metadata_type_id`,
			`user_id`,
			`code`
		) VALUES (
			'" . $_SESSION['user_id'] . "',
			" . $metadata_type_id . ",
			" . $user_id . ",
			'" . $rand . "'
		);";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function getRecordsBymetadataTypeAdmin($metadata_type_id)
	{
		$sql = 'SELECT * FROM metadata WHERE metadata_type_id = ' . $metadata_type_id . ' ORDER BY id;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM metadata WHERE 1 ORDER BY id;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}

	public function writeData($title, $image, $description, $keywords, $google, $facebook)
	{
		$sql =
		"UPDATE `metadata` SET
				modified_by = " . $_SESSION['user_id'] . ",
				title = '" . str_replace("'", "\'", $title) . "',
				image = '" . str_replace("'", "\'", $image) . "',
				description = '" . str_replace("'", "\'", $description) . "',
				keywords = '" . str_replace("'", "\'", $keywords) . "',
				google_analytics = '" . str_replace("'", "\'", $google) . "',
				facebook_graph = '" . str_replace("'", "\'", $facebook) . "'
			WHERE
				id = 1;";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `metadata` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}

}