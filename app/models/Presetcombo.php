<?php
class Presetcombo extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM preset_combo WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecent()
	{
		$sql = 'SELECT * FROM preset_combo WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}

	public function getRecords()
	{
		$sql = 'SELECT * FROM preset_combo WHERE deleted = 0 ORDER BY id DESC;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function writeData($id, $name)
	{
		if ($id > 0) {
			$sql =
			"UPDATE `preset_combo` SET
				modified_by = " . $_SESSION['user_id'] . ",
				name = '" . str_replace("'", "\'", $name) . "'
			WHERE
				id = " . str_replace("'", "\'", $id) . ";";
		} else {
			$sql =
			"INSERT INTO `preset_combo`
			(
				`created_by`, 
				`name`
			) VALUES (
				'" . $_SESSION['user_id'] . "',
				'" . str_replace("'", "\'", $name) . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `preset_combo` SET deleted = 1 WHERE id = ' . $id;

		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}