<?php
class Presetcombopart extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM preset_combo_part WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}

	public function getRecords()
	{
		$sql = 'SELECT * FROM preset_combo_part WHERE deleted = 0 ORDER BY id DESC;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByPresetComboId($preset_combo_id)
	{
		$sql = 'SELECT * FROM preset_combo_part WHERE preset_combo_id = ' . $preset_combo_id . ' AND deleted = 0 ORDER BY id DESC;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function writeData($id, $preset_combo_id, $product_id, $units)
	{
		if ($id > 0) {
			$sql =
			"UPDATE `preset_combo_part` SET
				modified_by = " . $_SESSION['user_id'] . ",
				preset_combo_id = '" . str_replace("'", "\'", $preset_combo_id) . "',
				product_id = '" . str_replace("'", "\'", $product_id) . "',
				units = '" . str_replace("'", "\'", $units) . "'
			WHERE
				id = " . str_replace("'", "\'", $id) . ";";
		} else {
			$sql =
			"INSERT INTO `preset_combo_part`
			(
				`created_by`, 
				`preset_combo_id`, 
				`product_id`, 
				`units`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . str_replace("'", "\'", trim($preset_combo_id)) . "', 
				'" . str_replace("'", "\'", $product_id) . "', 
				'" . str_replace("'", "\'", $units) . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `preset_combo_part` SET deleted = 1 WHERE id = ' . $id;

		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}