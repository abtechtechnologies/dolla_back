<?php
class Product extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$return = false;
		
		if (is_numeric($id)) {
			$sql = 'SELECT * FROM product WHERE id = ' . $id . ' AND deleted = 0;';
			
			$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
			$return = false;
			
			foreach ($db->result_array() as $row) {
				$return = $row;
			}
		}
		
		return $return;
	}
	
	public function search($string)
	{
		$string = strtolower(trim($string));
		
		$sql = 'SELECT * FROM product WHERE name LIKE "%' . str_replace('"', '', $string) . '%" OR name LIKE "%' . str_replace(' ', '', str_replace('"', '', $string)) . '%" AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecordAdmin($id)
	{
		$return = false;
		
		if (is_numeric($id)) {
			$sql = 'SELECT * FROM product WHERE id = ' . $id . ';';
			
			$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
			$return = false;
			
			foreach ($db->result_array() as $row) {
				$return = $row;
			}
		}
		
		return $return;
	}
	
	public function getRecentId()
	{
		$sql = 'SELECT * FROM product WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getRecent()
	{
		$sql = 'SELECT * FROM product WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecords($include_children = false)
	{

		$sql = 'SELECT * FROM product WHERE deleted = 0 AND active = 1 AND part = 0 ORDER BY id desc;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getParts()
	{
		
		$sql = 'SELECT * FROM product WHERE deleted = 0 AND part = 1 ORDER BY id desc;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsList()
	{
		
		$sql = 'SELECT * FROM product WHERE deleted = 0 AND active = 1 ORDER BY id desc;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		$return[0]['name'] = '- Please Select -';
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsBackend()
	{
		
		$sql = 'SELECT * FROM product WHERE deleted = 0 AND part = 0 ORDER BY id desc;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductSales()
	{
		$sql = 'SELECT * FROM product WHERE deleted = 0 ORDER BY id desc;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		$this->load->model('sale');
		$this->load->model('product');
		$this->load->model('productsize');
		$this->load->model('transaction');
		
		$product_sizes = $this->productsize->getRecordsAdmin();
		$products = $this->product->getRecordsAdmin();

		foreach ($return as $id => $data) {
			
			$sales = $this->sale->getRecordsByProductId($id);
			
			$sale_count = count($sales);
			
			$return[$id]['sale_count'] = $sale_count;
			
			//14 days
			$recent1 = array();
			$recent1_pending = array();
			//30 days
			$recent2 = array();
			$recent2_pending = array();
			
			$recent1_total = 0;
			$recent2_total = 0;
			$recent1_pending_total = 0;
			$recent2_pending_total = 0;
			$recent_ws1_total = 0;
			$recent_ws2_total = 0;
			$recent_pending_ws1_total = 0;
			$recent_pending_ws2_total = 0;
			$total = 0;
			$total_units = 0;
			$total_pending = 0;
			$total_pending_units = 0;

			foreach ($sales as $id2 => $data2) {
				
				if ($data2['product_size_id'] > 0) {
					$transaction = $this->transaction->getRecord($data2['transaction_id']);

					if ($transaction['changed_date'] == '0000-00-00 00:00:00') {
						if (strtotime($transaction['created_date']) > time() - 1209600) {
							if ($transaction['pending'] == 1) {
								$recent1_pending[$id2]['cost'] = $product_sizes[$data2['product_size_id']]['price'];
								
								$recent1_pending_total += $product_sizes[$data2['product_size_id']]['price'];
								$recent_pending_ws1_total += $products[$data2['product_id']]['unit_price_wholesale'] * $product_sizes[$data2['product_size_id']]['units'];
								$total_pending_units += $product_sizes[$data2['product_size_id']]['units'];
							} else {
								$recent1[$id2]['cost'] = $product_sizes[$data2['product_size_id']]['price'];
								
								$recent1_total += $product_sizes[$data2['product_size_id']]['price'];
								$recent_ws1_total += $products[$data2['product_id']]['unit_price_wholesale'] * $product_sizes[$data2['product_size_id']]['units'];
								$total_units += $product_sizes[$data2['product_size_id']]['units'];
							}
							
						}
						
						if (strtotime($transaction['created_date']) > time() - 2592000) {
							if ($transaction['pending'] == 1) {
								$recent2_pending[$id2]['cost'] = $product_sizes[$data2['product_size_id']]['price'];
								
								$recent2_pending_total += $product_sizes[$data2['product_size_id']]['price'];
								$recent_pending_ws2_total += $products[$data2['product_id']]['unit_price_wholesale'] * $product_sizes[$data2['product_size_id']]['units'];
								$total_pending_units += $product_sizes[$data2['product_size_id']]['units'];
							} else {
								$recent2[$id2]['cost'] = $product_sizes[$data2['product_size_id']]['price'];
								
								$recent2_total += $product_sizes[$data2['product_size_id']]['price'];
								$recent_ws2_total += $products[$data2['product_id']]['unit_price_wholesale'] * $product_sizes[$data2['product_size_id']]['units'];
								$total_units += $product_sizes[$data2['product_size_id']]['units'];
							}
							
						}
					} else {
						if (strtotime($transaction['changed_date']) > time() - 1209600) {
							if ($transaction['pending'] == 1) {
								$recent1_pending[$id2]['cost'] = $product_sizes[$data2['product_size_id']]['price'];
								
								$recent1_pending_total += $product_sizes[$data2['product_size_id']]['price'];
								$recent_pending_ws1_total += $products[$data2['product_id']]['unit_price_wholesale'] * $product_sizes[$data2['product_size_id']]['units'];
								$total_pending_units += $product_sizes[$data2['product_size_id']]['units'];
							} else {
								$recent1[$id2]['cost'] = $product_sizes[$data2['product_size_id']]['price'];
								
								$recent1_total += $product_sizes[$data2['product_size_id']]['price'];
								$recent_ws1_total += $products[$data2['product_id']]['unit_price_wholesale'] * $product_sizes[$data2['product_size_id']]['units'];
								$total_units += $product_sizes[$data2['product_size_id']]['units'];
							}
							
						}
						
						if (strtotime($transaction['changed_date']) > time() - 2592000) {
							if ($transaction['pending'] == 1) {
								$recent2_pending[$id2]['cost'] = $product_sizes[$data2['product_size_id']]['price'];
								
								$recent2_pending_total += $product_sizes[$data2['product_size_id']]['price'];
								$recent_pending_ws2_total += $products[$data2['product_id']]['unit_price_wholesale'] * $product_sizes[$data2['product_size_id']]['units'];
								$total_pending_units += $product_sizes[$data2['product_size_id']]['units'];
							} else {
								$recent2[$id2]['cost'] = $product_sizes[$data2['product_size_id']]['price'];
								
								$recent2_total += $product_sizes[$data2['product_size_id']]['price'];
								$recent_ws2_total += $products[$data2['product_id']]['unit_price_wholesale'] * $product_sizes[$data2['product_size_id']]['units'];
								$total_units += $product_sizes[$data2['product_size_id']]['units'];
							}
							
						}
					}
					
					if (isset($product_sizes[$data2['product_size_id']])) {
						if ($transaction['pending'] == 1) {
							$total_pending += $product_sizes[$data2['product_size_id']]['price'];
						} else {
							$total += $product_sizes[$data2['product_size_id']]['price'];
						}
					}
				} else {
					
				}
				
			}

			$return[$id]['recent1_total'] = $recent1_total;
			$return[$id]['recent2_total'] = $recent2_total;
			
			$return[$id]['recent_ws1_total'] = $recent_ws1_total;
			$return[$id]['recent_ws2_total'] = $recent_ws2_total;
			
			$return[$id]['recent1'] = $recent1;
			$return[$id]['recent2'] = $recent2;
			
			$return[$id]['total_sales'] = $total;
			$return[$id]['total_units'] = $total_units;
			$return[$id]['retail_unit_price'] = $this->productsize->getUnitPrice($id);
			
			if ($data['part'] == 1) {
				unset($return[$id]);
			}
		}

		return $return;
	}

	
	public function getRecordsAll()
	{
		
		$sql = 'SELECT * FROM product WHERE deleted = 0 ORDER BY id desc;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductsByProductTypeId()
	{
		
		$sql = 'SELECT * FROM product WHERE deleted = 0 AND active = 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (!isset($return[$row['product_type_id']])) {
				$return[$row['product_type_id']] = array();
			}
			
			$return[$row['product_type_id']][$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductsByVendorId($vendor_id)
	{
		
		$sql = 'SELECT * FROM product WHERE deleted = 0 AND active = 1 AND parent_id = 0 AND vendor_id = ' . $vendor_id. ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM product WHERE 1;';
		
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecentProductId()
	{
		$sql = 'SELECT * FROM product WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getProductsByProductType($product_type_id)
	{
		$sql = 'SELECT * FROM product WHERE deleted = 0 AND active = 1 AND product_type_id = ' . $product_type_id . ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductsByParentId($product_id)
	{
		$sql = 'SELECT * FROM product WHERE deleted = 0 AND parent_id = ' . $product_id . ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductsByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row['product_id'];
		}
		
		return $return;
	}
	
	public function getProductSizesByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE transaction_id = ' . $transaction_id . ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (isset($return[$row['product_size_id']])) {
				$return[$row['product_size_id']]['quantity'] = $return[$row['product_size_id']]['quantity'] + $row['quantity'];
			} else {
				$return[$row['product_size_id']] = $row;
			}
			
		}
		
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM product WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductSizes()
	{
		$sql = 'SELECT * FROM product WHERE deleted = 0 AND parent_id > 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function updateName($product_id, $name)
	{
		$product_id = str_replace("'", "\'", trim($product_id));
		$name = str_replace("'", "\'", trim($name));
		
		$sql =
		"UPDATE `product` SET
			modified_by = " . $_SESSION['user_id'] . ",
			name = '" . $name . "'
		WHERE
			id = " . $product_id . ";";

		
		$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function writeData($data)
	{
		if ($data['product_id'] > 0) {
			$sql =
			"UPDATE `product` SET
				modified_by = " . $_SESSION['user_id'] . ",
				name = '" . str_replace("'", "\'", trim($data['name'])) . "',
				description = '" . str_replace("'", "\'", trim($data['description'])) . "',
				product_type_id = '" . str_replace("'", "\'", trim($data['product_type_id'])) . "',
				unit_name = '" . str_replace("'", "\'", trim($data['unit_name'])) . "',
				vendor_id = '" . str_replace("'", "\'", trim($data['vendor_id'])) . "',
				price = 0,
				has_child = 1,
				parent_id = 0,
				unit_price_wholesale = '" . str_replace("'", "\'", trim($data['unit_price_wholesale'])) . "',
				combo = '" . str_replace("'", "\'", trim($data['combo'])) . "',
				inventory_holder_id = '" . str_replace("'", "\'", trim($data['inventory_holder_id'])) . "',
				attachment_file_name = '" . str_replace("'", "\'", $data['attachment']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['product_id']) . ";";
		} else {
			if (isset($data['part']) && $data['part'] == 1) {
				$part = 1;
			} else {
				$part = 0;
			}
			
			$sql =
			"INSERT INTO `product`
			(
				`created_by`,
				`name`,
				`description`,
				`product_type_id`,
				`inventory_holder_id`,
				`price`,
				`has_child`,
				`parent_id`,
				`unit_name`,
				`vendor_id`,
				`active`,
				`unit_price_wholesale`,
				`part`,
				`combo`,
				`attachment_file_name`
			) VALUES (
				'" . $_SESSION['user_id'] . "',
				'" . str_replace("'", "\'", trim($data['name'])) . "',
				'" . str_replace("'", "\'", trim($data['description'])) . "',
				'" . str_replace("'", "\'", trim($data['product_type_id'])) . "',
				'" . str_replace("'", "\'", trim($data['inventory_holder_id'])) . "',
				'0',
				'1',
				'0',
				'" . str_replace("'", "\'", trim($data['unit_name'])) . "',
				'" . str_replace("'", "\'", trim($data['vendor_id'])) . "',
				1,
				'" . str_replace("'", "\'", trim($data['unit_price_wholesale'])) . "',
				'" . str_replace("'", "\'", trim($part)) . "',
				'" . str_replace("'", "\'", trim($data['combo'])) . "',
				'" . str_replace("'", "\'", trim($data['attachment'])) . "'
			);";
		}
		
		$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `product` SET deleted = 1 WHERE id = ' . $id;
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		if ($status == 1) {
			$sql = 'UPDATE `inventory` SET deleted = 1 WHERE product_id = ' . $id;
			
			$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		}
		
		$this->load->model('productsize');
		$sizes = $this->productsize->getRecordsByProductId($id);
		
		foreach ($sizes as $product_size_id => $data) {
			if ($status == 1) {
				$status = $this->productsize->delete($product_size_id);
			}
		}
		
		return $status;
	}
	
	public function activate($data)
	{
		$sql = 'UPDATE `product` SET active = ' . $data['active'] . ' WHERE id = ' . $data['product_id'];
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function switchActive($product_id, $source)
	{
		$sql = 'UPDATE `product` SET active = ' . $source . ' WHERE id = ' . $product_id;
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function updateCache($id, $file)
	{
		$sql = 'UPDATE `product` SET thumb = \'' . $file . '\' WHERE id = ' . $id;
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function changePrimaryHolder($product_id, $user_id)
	{
		$sql = 'UPDATE `product` SET inventory_holder_id = ' . $user_id . ' WHERE id = ' . $product_id;
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
}