<?php
class Sale extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM sale WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM sale WHERE deleted = 0 ORDER BY id DESC;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getSalesByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE transaction_id = ' . $transaction_id . ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getActiveRecords()
	{
		$sql = 'SELECT * FROM sale WHERE delivered = 0 AND deleted = 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM sale WHERE user_id = ' . $id . ' AND deleted = 0 ORDER BY id DESC;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByTime($hours)
	{
	    $date = strtotime(date("Y-m-d H:m:s", strtotime('-' . $hours . ' hours', time())));

	    $sql = "SELECT * FROM sale WHERE deleted = 0 ORDER BY created_date DESC;";
	
	    $db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	    
	    foreach ($db->result_array() as $row) {
	        if (strtotime($row['created_date']) > $date) {
	            $return[$row['id']] = $row;
	        }
	    }
	
	    return $return;
	}
	
	public function getRecordsByDate($date)
	{
		$sql = "SELECT * FROM sale WHERE deleted = 0 ORDER BY created_date DESC;";
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$row_date = substr($row['created_date'], 0, 10);

			if ($date == $row_date) {
				$return[$row['id']] = $row;
			}
		}
	
		return $return;
	}
	
	public function getRecordsByDateRange($date1, $date2)
	{
		//format strings
		$new_date1 = explode('/', $date1);
		$new_date2 = explode('/', $date2);
		
		if (strlen($new_date1[0]) == 1) {
			$new_date1[0] = '0' . $new_date1[0];
		}
		
		if (strlen($new_date1[1]) == 1) {
			$new_date1[1] = '0' . $new_date1[1];
		}
		
		if (strlen($new_date2[0]) == 1) {
			$new_date2[0] = '0' . $new_date2[0];
		}
		
		if (strlen($new_date2[1]) == 1) {
			$new_date2[1] = '0' . $new_date2[1];
		}

		$new_date1 = $new_date1[2] . '-' . $new_date1[0] . '-' . $new_date1[1];
		$new_date2 = $new_date2[2] . '-' . $new_date2[0] . '-' . $new_date2[1];

		$sql = "SELECT * FROM sale WHERE deleted = 0 ORDER BY id DESC;";
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			$row_date = explode(' ', $row['created_date']);
			$row_date = $row_date[0];

			if ($new_date1 == $new_date2) {
				//1 Date
				if ($new_date1 == $row_date) {
					$return[$row['id']] = $row;
				}
			} else {
				//Date range
				$time1 = strtotime($new_date1);
				$time2 = strtotime($new_date2);
				
				$row_time = strtotime($row_date);
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['id']] = $row;
				}
			}
		}
		
		return $return;
	}
	
	public function getRecordsByTransactionId($id)
	{
		$sql = 'SELECT * FROM sale WHERE transaction_id = ' . $id . ' AND deleted = 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByProductId($product_id)
	{
		$sql = 'SELECT * FROM sale WHERE product_id = ' . $product_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function writeData($data)
	{
		if (isset($data['new_created_date']) && $data['new_created_date'] == '') {
			if ($data['sale_id'] > 0) {
				$sql =
				"UPDATE `sale` SET
					modified_by = " . $_SESSION['user_id'] . ",
					user_id = '" . str_replace("'", "\'", trim($data['user_id'])) . "',
					quantity = '" . str_replace("'", "\'", trim($data['quantity'])) . "',
					product_size_id = '" . str_replace("'", "\'", trim($data['product_size_id'])) . "',
					holder_user_id = '" . str_replace("'", "\'", trim($data['holder_user_id'])) . "',
					product_id = '" . str_replace("'", "\'", $data['product_id']) . "'
				WHERE
					id = " . str_replace("'", "\'", $data['sale_id']) . ";";
			} else {
				$sql =
				"INSERT INTO `sale`
				(
					`created_by`,
					`user_id`,
					`product_id`,
					`quantity`,
					`product_size_id`,
					`holder_user_id`,
					`transaction_id`
				) VALUES (
					'" . $_SESSION['user_id'] . "',
					'" . str_replace("'", "\'", trim($data['user_id'])) . "',
					'" . str_replace("'", "\'", trim($data['product_id'])) . "',
					'" . str_replace("'", "\'", trim($data['quantity'])) . "',
					'" . str_replace("'", "\'", trim($data['product_size_id'])) . "',
					'" . str_replace("'", "\'", trim($data['holder_user_id'])) . "',
					'" . str_replace("'", "\'", trim($data['transaction_id'])) . "'
				);";
			}
		} else {
			if ($data['sale_id'] > 0) {
				$sql =
				"UPDATE `sale` SET
					created_date = " . $data['new_created_date'] . ",
					modified_by = " . $_SESSION['user_id'] . ",
					user_id = '" . str_replace("'", "\'", trim($data['user_id'])) . "',
					quantity = '" . str_replace("'", "\'", trim($data['quantity'])) . "',
					product_size_id = '" . str_replace("'", "\'", trim($data['product_size_id'])) . "',
					holder_user_id = '" . str_replace("'", "\'", trim($data['holder_user_id'])) . "',
					product_id = '" . str_replace("'", "\'", $data['product_id']) . "'
				WHERE
					id = " . str_replace("'", "\'", $data['sale_id']) . ";";
			} else {
				$sql =
				"INSERT INTO `sale`
				(
					`created_date`,
					`created_by`,
					`user_id`,
					`product_id`,
					`quantity`,
					`product_size_id`,
					`holder_user_id`,
					`transaction_id`
				) VALUES (
					'" . $data['new_created_date'] . "',
					'" . $_SESSION['user_id'] . "',
					'" . str_replace("'", "\'", trim($data['user_id'])) . "',
					'" . str_replace("'", "\'", trim($data['product_id'])) . "',
					'" . str_replace("'", "\'", trim($data['quantity'])) . "',
					'" . str_replace("'", "\'", trim($data['product_size_id'])) . "',
					'" . str_replace("'", "\'", trim($data['holder_user_id'])) . "',
					'" . str_replace("'", "\'", trim($data['transaction_id'])) . "'
				);";
			}
		}

		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `sale` SET deleted = 1 WHERE id = ' . $id;

		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}