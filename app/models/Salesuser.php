<?php
class Salesuser extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getRecent()
	{
		$sql = 'SELECT * FROM salesuser WHERE 1 ORDER BY id DESC LIMIT 1';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecentId()
	{
		$sql = 'SELECT * FROM salesuser WHERE 1 ORDER BY id DESC LIMIT 1';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = 0;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getRecords()
	{
		$DB = $this->load->database('abtech', TRUE);
		
		$sql = 'SELECT * FROM salesuser WHERE 1 ORDER BY id DESC;';
		
		$db = $DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM salesuser WHERE id = ' . $id;
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecordAdmin($id)
	{
		$sql = 'SELECT * FROM salesuser WHERE id = ' . $id;
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin($reverse = false)
	{
		if ($reverse == true) {
			$sql = 'SELECT * FROM salesuser WHERE 1 ORDER BY id DESC;';
		} else {
			$sql = 'SELECT * FROM salesuser WHERE 1;';
		}
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}

	public function search($query, $member)
	{
		$sql = "
			SELECT * FROM salesuser WHERE
			first_name LIKE '%" . $query . "%'
			OR last_name LIKE '%" . $query . "%'
			ORDER BY id DESC;";
		
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			if ($member == true) {
				if ($row['membership'] > 0) {
					$return[$row['id']] = $row;
				}
			} else {
				$return[$row['id']] = $row;
			}
		}
	
		return $return;
	}

	public function delete($id)
	{
		$sql = 'UPDATE `salesuser` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}