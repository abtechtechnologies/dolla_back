<?php
class Vendor extends CI_Model {

	public function __construct()
	{
		$this->load->database();

		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM vendor WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecentId()
	{
		$sql = 'SELECT * FROM vendor ORDER BY id DESC LIMIT 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = 0;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	
	public function getRecords($include_blank = false)
	{				
		$sql = 'SELECT * FROM vendor WHERE deleted = 0 ORDER BY id;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		if ($include_blank == true) {
			$return[0] = array();
			$return[0]['name'] = 'None';
			
		}
				
		foreach ($db->result_array() as $row) {
			
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordByEmail($email)
	{
		$sql = 'SELECT * FROM vendor WHERE deleted = 0 AND email = ' . '\'' . $email . '\'';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getFeatured()
	{
		$sql = 'SELECT * FROM vendor WHERE featured > 0 AND deleted = 0 ORDER BY id;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM vendor WHERE 1;';
	
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecentvendorId()
	{
		$sql = 'SELECT * FROM vendor WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getvendorsByvendorType($vendor_type_id)
	{
		$sql = 'SELECT * FROM vendor WHERE deleted = 0 AND parent_id = 0 AND vendor_type_id = ' . $vendor_type_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getvendorsByParentId($vendor_id)
	{
		$sql = 'SELECT * FROM vendor WHERE deleted = 0 AND parent_id = ' . $vendor_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getvendorsByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[] = $row['vendor_id'];
		}
	
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM vendor WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getvendorSizes()
	{
		$sql = 'SELECT * FROM vendor WHERE deleted = 0 AND parent_id > 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function writeData($id, $name, $title, $phone, $phone_show, $address, $description, $website, $facebook, $twitter, $instagram, $vendor_type_id, $email)
	{
		if ($id > 0) {
			$sql =
			"UPDATE `vendor` SET
				modified_by = " . $_SESSION['user_id'] . ",
				name = '" . str_replace("'", "\'", trim($name)) . "',
				title = '" . str_replace("'", "\'", trim($title)) . "',
				phone = '" . str_replace("'", "\'", trim($phone)) . "',
				phone_show = '" . str_replace("'", "\'", trim($phone_show)) . "',
				address = '" . str_replace("'", "\'", trim($address)) . "',
				description = '" . str_replace("'", "\'", trim($description)) . "',
				website = '" . str_replace("'", "\'", trim($website)) . "',
				facebook = '" . str_replace("'", "\'", trim($facebook)) . "',
				twitter = '" . str_replace("'", "\'", trim($twitter)) . "',
				instagram = '" . str_replace("'", "\'", trim($instagram)) . "',
				vendor_type_id = '" . str_replace("'", "\'", trim($vendor_type_id)) . "',
				email = '" . str_replace("'", "\'", $email) . "'
			WHERE
				id = " . str_replace("'", "\'", $id) . ";";
		} else {
			$sql =
			"INSERT INTO `vendor`
			(
				`created_by`, 
				`rep_name`, 
				`phone`, 
				`address`, 
				`website`, 
				`name`, 
				`description`, 
				`attachment_file_name`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . str_replace("'", "\'", trim($data['rep_name'])) . "', 
				'" . str_replace("'", "\'", trim($data['phone'])) . "', 
				'" . str_replace("'", "\'", trim($data['address'])) . "',
				'" . str_replace("'", "\'", trim($data['website'])) . "', 
				'" . str_replace("'", "\'", trim($data['name'])) . "', 
				'" . str_replace("'", "\'", trim($data['description'])) . "', 
				'" . str_replace("'", "\'", trim($data['attachment'])) . "'
			);";
		}
		
		$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
	
	public function writeProfileImage($vendor_id, $file_name)
	{
		$sql =
		"UPDATE `vendor` SET
				attachment_file_name = '" . str_replace("'", "\'", $file_name) . "'
			WHERE
				id = " . str_replace("'", "\'", $vendor_id) . ";";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function signUp($email, $unlock, $name, $vendor_type_id)
	{
		// A higher "cost" is more secure but consumes more processing power
		$cost = 10;
		
		// Create a random salt
		$salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
		
		// Prefix information about the hash so PHP knows how to verify it later.
		// "$2a$" Means we're using the Blowfish algorithm. The following two digits are the cost parameter.
		$salt = sprintf("$2a$%02d$", $cost) . $salt;
		
		// Hash the password with the salt
		
		//***TOGGLE ENCRYPTION***
		//$unlock = crypt($unlock, $salt);
		
		$this->load->model('_utility');
		$file_name = $this->_utility->cleanString($name);
		
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();

		if (!file_exists('./uploads/clients/' . $company['db_name'] . '/vendors/' . $file_name)) {
			$status = mkdir('./uploads/clients/' . $company['db_name'] . '/vendors/' . $file_name, 0777, true);
		} else {
			$status = 'Duplicate company name found';
		}
		
		if ($status == 1) {
			$sql =
			"INSERT INTO `vendor`
			(
				`created_by`,
				`email`,
				`unlock`,
				`file_name`,
				`vendor_type_id`,
				`name`
			) VALUES (
				1,
				'" . str_replace("'", "\'", trim($email)) . "',
				'" . str_replace("'", "\'", trim($unlock)) . "',
				'" . str_replace("'", "\'", trim($file_name)) . "',
				'" . str_replace("'", "\'", trim($vendor_type_id)) . "',
				'" . str_replace("'", "\'", trim($name)) . "'
			);";
			
			$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		}
		
		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `vendor` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		if ($status == 1) {
			$sql = 'UPDATE `inventory` SET deleted = 1 WHERE vendor_id = ' . $id;
			
			$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		}
	
		return $status;
	}
}