<?php

class _admin extends CI_Model {
	
	public function load($page_data)
	{
		$this->load->model('user');
		
		$page_data['loader']['uri'] = $_SERVER['REQUEST_URI'];
		
		//Strip first '/' in URI
		$page = substr($_SERVER['REQUEST_URI'], 1);
		
		//Strip page vars
		if (strpos($page, "?") != false) {
			$page = substr($page, 0, strpos($page, "?"));
		}
		
		//Check if client
		if ($_SESSION['client_id'] > 1) {
			//Find position of next '/'
			$dash_position = strpos($page, '/');
			$page = substr($page, $dash_position + 1);
		}
		
		//If empty string, then you are at the home page
		if (strlen($page) == 0) {
			$page = 'home/index';
		} else {
			//Find position of next '/'
			$dash_position = strpos($page, '/');
			
			$sub = substr($page, $dash_position + 1);
			$tac_position = strpos($sub, '-');
			
			$content_type = substr($sub, $tac_position + 1);
			
			$question_position = strpos($content_type, '?');
			
			//Strip page input variables
			if ($question_position > 0) {
				$page_data['loader']['content_type'] = substr($content_type, 0, $question_position);
			} else {
				$page_data['loader']['content_type'] = $content_type;
			}
			
			if (($dash_position + 1) == strlen($page)) {
				$page = substr($page, 0, $dash_position);
				$dash_position = '';
			}
			
			//If it doesnt exist, you are at an index page
			if ($dash_position == '') {
				$page = $page . '/index';
			}
		}
		
		$this->load->model('companyinfo');
		$page_data['company_info'] = $this->companyinfo->getRecord();
		
		$page_data['loader']['admins'] = $this->user->getAdmins();
		
		$this->load->model('adminmodule');
		$page_data['loader']['modules'] = $this->adminmodule->getRecords();
		
		$page_data['loader']['dev_mode'] = $page_data['company_info']['dev_mode'];
		
		$page_data['loader']['inactive'] = $this->user->getInactive();
		
		$this->load->helper('url');
		
		if ($_SESSION['client_id'] == 1) {
			if (!isset($_SESSION['admin_user_id']) || $_SESSION['admin_user_id'] == 0) {
				
				if ($page != 'admin/login') {
					//Don't allow access without a logged in user
					//@todo
					redirect('http://' . $page_data['company_info']['site'] . '/admin/login');
				}
				
			} else {
				//If user is logged in, check if its an admin
				
				$page_data['loader']['user'] = $this->user->getRecord($_SESSION['user_id']);
				$page_data['loader']['admin_user'] = $this->user->getRecord($_SESSION['admin_user_id']);
				
				//1 => admin, 3 => delivery drivers, 4 => phone operators
				if ($page_data['loader']['admin_user']['user_type_id'] !== '1' && $page_data['loader']['admin_user']['user_type_id'] != 3 && $page_data['loader']['admin_user']['user_type_id'] != 4 && $page_data['loader']['admin_user']['user_type_id'] != 5 && $page_data['loader']['admin_user']['user_type_id'] != 6) {
					$_SESSION['user_id'] = 0;
					$_SESSION['admin_user_id'] = 0;
					redirect('http://' . $page_data['company_info']['site']);
				}
			}
		} else {
			$this->load->model('client');
			$client = $this->client->getRecord($_SESSION['client_id']);
			
			$session_user_var = $client['db_name'] . 'user_id';
			$session_admin_user_var = $client['db_name'] . 'admin_user_id';
			
			if (!isset($_SESSION[$session_admin_user_var]) || $_SESSION[$session_admin_user_var] == 0) {
				
				if ($page != 'admin/login') {
					//Don't allow access without a logged in user
					//@todo
					redirect('http://' . $page_data['company_info']['site'] . '/_' . $client['db_name'] . '/admin/login');
				}
				
			} else {
				//If user is logged in, check if its an admin
				
				$page_data['loader']['user'] = $this->user->getRecord($_SESSION[$session_user_var]);
				$page_data['loader']['admin_user'] = $this->user->getRecord($_SESSION[$session_admin_user_var]);
				
				//1 => admin, 3 => delivery drivers, 4 => phone operators
				if ($page_data['loader']['admin_user']['user_type_id'] !== '1' && $page_data['loader']['admin_user']['user_type_id'] != 3 && $page_data['loader']['admin_user']['user_type_id'] != 4 && $page_data['loader']['admin_user']['user_type_id'] != 5 && $page_data['loader']['admin_user']['user_type_id'] != 6) {
					$_SESSION[$session_user_var] = 0;
					$_SESSION[$session_admin_user_var] = 0;
					redirect('http://' . $page_data['company_info']['site']);
				}
			}
		}
		
		if (isset($_SESSION['admin_user_id']) && $_SESSION['admin_user_id'] > 0) {
			$_SESSION['admin_user'] = $this->user->getRecord($_SESSION['admin_user_id']);
		}
		
		//@todo save in database
		$page_data['loader']['admin_user_type_id'] = 1;
		
		$page_data['loader']['page'] = $page;
				
		$this->load->view('../../admin/header.phtml', $page_data);
		$this->load->view($page . '.phtml', $page_data);
		$this->load->view('../../admin/footer.phtml', $page_data);
		
		return $page_data;
	}
}
