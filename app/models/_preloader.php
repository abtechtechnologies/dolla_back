<?php

class _preloader extends CI_Model {
	
	public function load($action = false)
	{

		//Strip first '/' in URI
		$page = substr($_SERVER['REQUEST_URI'], 1);
		
		//Strip page vars
		if (strpos($page, "?") != false) {
			$page_var = substr($page, strpos($page, "?") + 1);
			$page = substr($page, 0, strpos($page, "?"));
		}

		$page_data['loader']['page'] = $page;
		
		if (isset($_SESSION['client_id']) && $_SESSION['client_id'] > 0 && $action == true) {
			
		} else {
			//Check if client or main site
			if (strpos($page, "_") === 0) {
				//Find position of next '/'
				$dash_position = strpos($page, '/');

				if ($dash_position != false) {
					//This is a client
					$client_db_name = substr($page, 1, $dash_position - 1);
				} else {
					//This is a client
					$client_db_name = substr($page, 1);
				}
				
				$this->load->model('client');

				$match = $this->client->getRecordByDbName($client_db_name);
				
				if (count($match) > 0) {
					$_SESSION['client_id'] = $match['id'];
				} else {
					$_SESSION['client_id'] = 0;
				}
			} else {
				$_SESSION['client_id'] = 1;
			}
		}

		$client_id = $_SESSION['client_id'];

		$return = array(
			'client_id' => $client_id
		);
	
		return $return;
	}
}
