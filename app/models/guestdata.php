<?php
class Guestdata extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getRecent()
	{
		$sql = 'SELECT * FROM guestdata WHERE 1 ORDER BY id DESC LIMIT 1';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecentId()
	{
		$sql = 'SELECT * FROM guestdata WHERE 1 ORDER BY id DESC LIMIT 1';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = 0;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getRecords()
	{
		$DB = $this->load->database('abtech', TRUE);
		
		$sql = 'SELECT * FROM guestdata WHERE 1 ORDER BY id DESC;';
		
		$db = $DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM guestdata WHERE id = ' . $id;
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecordAdmin($id)
	{
		$sql = 'SELECT * FROM guestdata WHERE id = ' . $id;
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin($reverse = false)
	{
		if ($reverse == true) {
			$sql = 'SELECT * FROM guestdata WHERE 1 ORDER BY id DESC;';
		} else {
			$sql = 'SELECT * FROM guestdata WHERE 1;';
		}
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByTime($hours)
	{
		$DB = $this->load->database('abtech', TRUE);
		
		$date = strtotime(date("Y-m-d H:m:s", strtotime('-' . $hours . ' hours', time())));
		
		$sql = "SELECT * FROM guestdata WHERE 1 ORDER BY time DESC;";
		
		$db = $DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (strtotime($row['time']) > $date) {
				$return[$row['id']] = $row;
			}
		}
		
		return $return;
	}
	
	public function getRecordsByDate($date)
	{
		$DB = $this->load->database('abtech', TRUE);
		
		$sql = "SELECT * FROM guestdata WHERE 1 ORDER BY time DESC;";
		
		$db = $DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$row_date = substr($row['time'], 0, 10);
			
			if ($date == $row_date) {
				$return[$row['id']] = $row;
			}
		}
		
		return $return;
	}
	
	public function getRecordsByDateRange($date1, $date2)
	{
		$DB = $this->load->database('abtech', TRUE);
		
		//format strings
		$new_date1 = substr($date1, 6) . '-' . substr($date1, 0, 2) . '-' . substr($date1, 3, 2);
		$new_date2 = substr($date2, 6) . '-' . substr($date2, 0, 2) . '-' . substr($date2, 3, 2);
		
		$sql = "SELECT * FROM guestdata WHERE 1 ORDER BY time DESC;";
		
		$db = $DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$row_date = substr($row['time'], 0, 10);
			
			if ($new_date1 == $new_date2) {
				//1 Date
				if ($new_date1 == $row_date) {
					$return[$row['id']] = $row;
				}
			} else {
				//Date range
				$time1 = strtotime($new_date1);
				$time2 = strtotime($new_date2);
				
				$row_time = strtotime($row_date);
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['id']] = $row;
				}
			}
		}
		
		return $return;
	}
	
	public function search($query, $member)
	{
		$sql = "
			SELECT * FROM guestdata WHERE
			first_name LIKE '%" . $query . "%'
			OR last_name LIKE '%" . $query . "%'
			OR phone LIKE '%" . $query . "%'
			ORDER BY id DESC;";
		
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			if ($member == true) {
				if ($row['membership'] > 0) {
					$return[$row['id']] = $row;
				}
			} else {
				$return[$row['id']] = $row;
			}
		}
	
		return $return;
	}

	public function delete($id)
	{
		$sql = 'UPDATE `guestdata` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}